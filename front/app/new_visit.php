<?php include('includes/header.php'); ?>


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="card ">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-card-center"> زيارة جديدة </h4>
                </div>
                <form class="form">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-section-head">
                                <h4> معلومات الزيارة </h4>
                            </div>

                            <div class="form-group">
                                <div class="row">

                                    <div class="clear"></div>
                                    <div class="col-md-6">
                                        <label>اسم الطبيب</label>
                                        <select class="form-control input-sm">
                                            <option>doc 1</option>
                                            <option>doc 2</option>
                                            <option>doc 3</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label> رقم الملف</label>
                                        <input type="text" class="form-control input-sm" name="" value="55">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="timesheetinput3"> التاريخ </label>
                                        <div class="position-relative has-icon-left">
                                            <input type="date" id="timesheetinput3" class="form-control input-sm"
                                                   name="date">
                                            <div class="form-control-position">
                                                <i class="icon-calendar5"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-rest-left">
                                        <label>ساعة الحضور </label>
                                        <input type="time" class="form-control input-sm" name="">
                                    </div>
                                    <div class="col-md-3 col-rest-right ">
                                        <label>ساعة المغادرة </label>
                                        <input type="time" class="form-control input-sm" name="">
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="timesheetinput3"> الزيارة القادمة </label>
                                        <div class="position-relative has-icon-left">
                                            <input type="date" id="timesheetinput3" class="form-control input-sm"
                                                   name="date">
                                            <div class="form-control-position">
                                                <i class="icon-calendar5"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>الساعة </label>
                                        <input type="time" class="form-control input-sm" name="">
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>

                        </div>
                        <div class="col-md-5 ">
                            <div class="form-section-head">
                                <h4>  الفحوص السريرية</h4>
                            </div>

                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="col-md-12">
                        <hr>
                        <div class="col-md-6">
                            <div class="form-section-head">
                                <h4> الاعراض والتقارير </h4>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2"> الشكوى </label>
                                <div class="col-md-10">
                                    <textarea class="form-control  "></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2"> الاعراض </label>
                                <div class="col-md-10">
                                    <textarea class="form-control  "></textarea>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2"> التقارير المخبرية </label>
                                <div class="col-md-10">
                                    <textarea class="form-control  "></textarea>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2">  تقارير الاشعة </label>
                                <div class="col-md-10">
                                    <textarea class="form-control  "></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-section-head">
                                <h4> التشخيص والعلاج </h4>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2"> التشخيص النهائي </label>
                                <div class="col-md-10">
                                    <textarea class="form-control  "></textarea>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2"> العلاج المقترح </label>
                                <div class="col-md-10">
                                    <textarea class="form-control  "></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2"> استشارة أخصائي </label>
                                <div class="col-md-10">
                                    <textarea class="form-control  "></textarea>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-2">  ملاحظات الطبيب </label>
                                <div class="col-md-10">
                                    <textarea class="form-control  "></textarea>
                                </div>
                            </div>


                        </div>
                    </div>

                    <div class="col-md-12">
                        <hr>
                        <div class="col-md-5 ">
                            <div class="form-section-head">
                                <h4> الفاتورة </h4>
                            </div>

                            <div class="form-group">
                                <label> شركة التامين </label>
                                <input type="text" class="form-control input-sm" name="" value="التكافل">
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label> المبلغ المطلوب </label>
                                        <div class="position-relative has-icon-right">
                                            <input type="text" id="" class="form-control input-sm" placeholder=""
                                                   name="">
                                            <div class="form-control-position">
                                                <span class="currency-icon-sm">NIS</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label> المبلغ المدفوع </label>
                                        <div class="position-relative has-icon-right">
                                            <input type="text" id="" class="form-control input-sm" placeholder=""
                                                   name="">
                                            <div class="form-control-position">
                                                <span class="currency-icon-sm">NIS</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label> مختبرات </label>
                                        <div class="position-relative has-icon-right">
                                            <input type="text" id="" class="form-control input-sm" placeholder=""
                                                   name="">
                                            <div class="form-control-position">
                                                <span class="currency-icon-sm">NIS</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label> كشفية </label>
                                        <div class="position-relative has-icon-right">
                                            <input type="text" id="" class="form-control input-sm" placeholder=""
                                                   name="">
                                            <div class="form-control-position">
                                                <span class="currency-icon-sm">NIS</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for=""> غير ذلك </label>
                                        <div class="position-relative has-icon-right">
                                            <input type="text" id="" class="form-control input-sm" placeholder=""
                                                   name="">
                                            <div class="form-control-position">
                                                <span class="currency-icon-sm">NIS</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label> البيان </label>
                                        <input type="text" class="form-control input-sm" name="" value="">
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                    </div>


                    <div class="clearfix"></div>
                    <div class="form-actions col-md-12">

                        <button type="submit" class="btn btn-primary">
                            <i class="icon-check2"></i> حفظ
                        </button>

                        <button type="button" class="btn btn-warning mr-1">
                            <i class="icon-cross2"></i> الغاء
                        </button>

                    </div>
                    <div class="clearfix"></div>
                </form>

                <div class="clearfix"></div>
            </section>
        </div>
    </div>
</div>

<?php include('includes/footer.php'); ?>
