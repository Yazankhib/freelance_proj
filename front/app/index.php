<?php include('includes/header.php'); ?>


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="row">
                <div class="col-xl-8 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title"> المواعيد </h4>
                            <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                            <div class="heading-elements">
                                <ul class="list-inline mb-0">
                                    <li><a data-action="reload"><i class="icon-reload"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover mb-0">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> المريض</th>
                                        <th> الطبيب</th>
                                        <th> التاريخ</th>
                                        <th>عمليات</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td class="text-truncate">ReactJS App</td>
                                        <td class="text-truncate">
                                            Sarah W.
                                        </td>
                                        <td class="text-truncate">
                                            <span class="tag tag-danger">
                                               10:30AM on 20 September, 2017
                                            </span>
                                        </td>
                                        <td>
                                            <a href="#" class="tb-icon text-danger"> <i
                                                        class="icon-bin font-large-0"></i>
                                            </a>
                                            <a href="#" class="tb-icon text-warning"> <i
                                                        class="icon-pencil22  font-large-0"></i> </a>
                                            <a href="#" class="tb-icon text-primary"> <i
                                                        class="icon-eye3  font-large-0"></i> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2 </td>
                                        <td class="text-truncate">ReactJS App</td>
                                        <td class="text-truncate">
                                            Sarah W.
                                        </td>
                                        <td class="text-truncate">
                                            <span class="tag  tag-danger">
                                               10:30AM on 20 September, 2017
                                            </span>
                                        </td>
                                        <td>
                                            <a href="#" class="tb-icon text-danger"> <i
                                                        class="icon-bin font-large-0"></i>
                                            </a>
                                            <a href="#" class="tb-icon text-warning"> <i
                                                        class="icon-pencil22  font-large-0"></i> </a>
                                            <a href="#" class="tb-icon text-primary"> <i
                                                        class="icon-eye3  font-large-0"></i> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3 </td>
                                        <td class="text-truncate">ReactJS App</td>
                                        <td class="text-truncate">
                                            Sarah W.
                                        </td>
                                        <td class="text-truncate">
                                            <span class="tag tag-warning">
                                               10:30AM on 22 September, 2017
                                            </span>
                                        </td>
                                        <td>
                                            <a href="#" class="tb-icon text-danger"> <i
                                                        class="icon-bin font-large-0"></i>
                                            </a>
                                            <a href="#" class="tb-icon text-warning"> <i
                                                        class="icon-pencil22  font-large-0"></i> </a>
                                            <a href="#" class="tb-icon text-primary"> <i
                                                        class="icon-eye3  font-large-0"></i> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4 </td>
                                        <td class="text-truncate">ReactJS App</td>
                                        <td class="text-truncate">
                                            Sarah W.
                                        </td>
                                        <td class="text-truncate">
                                            <span class="tag tag-success">
                                               10:30AM on 28 September, 2017
                                            </span>
                                        </td>
                                        <td>
                                            <a href="#" class="tb-icon text-danger"> <i
                                                        class="icon-bin font-large-0"></i>
                                            </a>
                                            <a href="#" class="tb-icon text-warning"> <i
                                                        class="icon-pencil22  font-large-0"></i> </a>
                                            <a href="#" class="tb-icon text-primary"> <i
                                                        class="icon-eye3  font-large-0"></i> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td> 5 </td>
                                        <td class="text-truncate">ReactJS App</td>
                                        <td class="text-truncate">
                                            Sarah W.
                                        </td>
                                        <td class="text-truncate">
                                            <span class="tag tag-success">
                                               10:30AM on 24 September, 2017
                                            </span>
                                        </td>
                                        <td>
                                            <a href="#" class="tb-icon text-danger"> <i
                                                        class="icon-bin font-large-0"></i>
                                            </a>
                                            <a href="#" class="tb-icon text-warning"> <i
                                                        class="icon-pencil22  font-large-0"></i> </a>
                                            <a href="#" class="tb-icon text-primary"> <i
                                                        class="icon-eye3  font-large-0"></i> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6 </td>
                                        <td class="text-truncate">ReactJS App</td>
                                        <td class="text-truncate">
                                            Sarah W.
                                        </td>
                                        <td class="text-truncate">
                                            <span class="tag tag-success">
                                               10:30AM on 23 September, 2017
                                            </span>
                                        </td>
                                        <td>
                                            <a href="#" class="tb-icon text-danger"> <i
                                                        class="icon-bin font-large-0"></i>
                                            </a>
                                            <a href="#" class="tb-icon text-warning"> <i
                                                        class="icon-pencil22  font-large-0"></i> </a>
                                            <a href="#" class="tb-icon text-primary"> <i
                                                        class="icon-eye3  font-large-0"></i> </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7 </td>
                                        <td class="text-truncate">ReactJS App</td>
                                        <td class="text-truncate">
                                            Sarah W.
                                        </td>
                                        <td class="text-truncate">
                                            <span class="tag tag-success">
                                               10:30AM on 23 September, 2017
                                            </span>
                                        </td>
                                        <td>
                                            <a href="#" class="tb-icon text-danger"> <i
                                                        class="icon-bin font-large-0"></i>
                                            </a>
                                            <a href="#" class="tb-icon text-warning"> <i
                                                        class="icon-pencil22  font-large-0"></i> </a>
                                            <a href="#" class="tb-icon text-primary"> <i
                                                        class="icon-eye3  font-large-0"></i> </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="pink">278</h3>
                                        <span> المستندات </span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-bag2 pink font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="teal">156</h3>
                                        <span> المرضى </span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-user1 teal font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="deep-orange">64</h3>
                                        <span> التقارير </span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-diagram deep-orange font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="card-block">
                                <div class="media">
                                    <div class="media-body text-xs-left">
                                        <h3 class="cyan">11</h3>
                                        <span> المواعيد</span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <i class="icon-ios-alarm-outline cyan font-large-2 float-xs-right"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
