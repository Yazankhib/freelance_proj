<?php include('includes/header.php'); ?>
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="col-xl-12 col-lg-12 col-xs-12">
                <div class="card patient-top-card">
                    <div class="card-body">
                        <div class="media">
                            <div class="p-2 text-xs-center bg-cyan bg-darken-2 media-left media-middle">
                                <i class="icon-user1 font-large-2 white"></i>
                            </div>
                            <div class="p-2 bg-cyan white media-body">
                                <h5> Ahmad Ahmad</h5>
                                <h5 class="text-bold-400">العمر : 55</h5>
                                <h5 class="text-bold-400">الحساسية : something</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            <section class="card">
                <ul class="nav nav-inline  nav-tabs nav-justified">
                    <li class="nav-item">
                        <a class="nav-link active" id="active-tab" data-toggle="tab" href="#tab1" aria-controls="active"
                           aria-expanded="true">
                            معلومات المريض
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab2" aria-controls="link" aria-expanded="false">
                            زيارات المريض
                        </a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab3" aria-controls="linkOpt">CDC المريض</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab4" aria-controls="linkOpt"> طعومات المريض </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab5" aria-controls="linkOpt"> تقرير </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab6" aria-controls="linkOpt"> تقرير طبي
                            للاجازة </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab7" aria-controls="linkOpt">زيارات الحامل</a>
                    </li>
                </ul>

                <div class="tab-content px-1 pt-1">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab1" aria-labelledby="active-tab"
                         aria-expanded="true">

                      <?php include ('patient_info.php')?>

                    </div>

                    <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="link-tab"
                         aria-expanded="false">
                        <div class="table-responsive">


                            <a href="new_visit.php" class=" button btn btn-outline-success">
                                اضافة زيارة +
                            </a>

                            <table class="table mb-0">
                                <thead class="bg-teal bg-lighten-4">
                                <tr>
                                    <th>#</th>
                                    <th>التاريخ</th>
                                    <th>الشكوى</th>
                                    <th>التشخيص</th>
                                    <th>العلاج</th>
                                    <th>عمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>01-10-2017</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>@mdo</td>
                                    <td>
                                        <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                        </a>
                                        <a href="#" class="tb-icon text-warning"> <i
                                                    class="icon-pencil22  font-large-0"></i> </a>
                                        <a href="#" class="tb-icon text-primary"> <i
                                                    class="icon-eye3  font-large-0"></i> </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>01-10-2017</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                    <td>@fat</td>
                                    <td>
                                        <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                        </a>
                                        <a href="#" class="tb-icon text-warning"> <i
                                                    class="icon-pencil22  font-large-0"></i> </a>
                                        <a href="#" class="tb-icon text-primary"> <i
                                                    class="icon-eye3  font-large-0"></i> </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>01-10-2017</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    <td>@twitter</td>
                                    <td>
                                        <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                        </a>
                                        <a href="#" class="tb-icon text-warning"> <i
                                                    class="icon-pencil22  font-large-0"></i> </a>
                                        <a href="#" class="tb-icon text-primary"> <i
                                                    class="icon-eye3  font-large-0"></i> </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="dropdownOpt1-tab"
                         aria-expanded="false">
                        tab 3
                    </div>


                    <div class="tab-pane fade" id="tab4" role="tabpanel" aria-labelledby="dropdownOpt1-tab">

                        <div class="table-responsive">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal"
                                    data-target="#addVaccination">
                                اضافة طعم +
                            </button>

                            <table class="table mb-0">
                                <thead class="bg-teal bg-lighten-4">
                                <tr>
                                    <th>#</th>
                                    <th>التاريخ</th>
                                    <th> الطعم</th>
                                    <th>الملاحظات</th>
                                    <th>عمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>01-10-2017</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>
                                        <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                        </a>
                                        <a href="#" class="tb-icon text-warning"> <i
                                                    class="icon-pencil22  font-large-0"></i> </a>
                                        <a href="#" class="tb-icon text-primary"> <i
                                                    class="icon-eye3  font-large-0"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>01-10-2017</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                    <td>
                                        <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                        </a>
                                        <a href="#" class="tb-icon text-warning"> <i
                                                    class="icon-pencil22  font-large-0"></i> </a>
                                        <a href="#" class="tb-icon text-primary"> <i
                                                    class="icon-eye3  font-large-0"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>01-10-2017</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    <td>
                                        <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                        </a>
                                        <a href="#" class="tb-icon text-warning"> <i
                                                    class="icon-pencil22  font-large-0"></i> </a>
                                        <a href="#" class="tb-icon text-primary"> <i
                                                    class="icon-eye3  font-large-0"></i>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div class="tab-pane fade" id="tab5" role="tabpanel" aria-labelledby="dropdownOpt1-tab">
                        <?php include('report.php') ?>
                    </div>


                    <div class="tab-pane fade" id="tab6" role="tabpanel" aria-labelledby="dropdownOpt1-tab">
                        <?php include('medical_report.php') ?>

                    </div>

                    <div class="tab-pane fade" id="tab7" role="tabpanel" aria-labelledby="dropdownOpt1-tab">

                        <?php include('pregnant_visits.php') ?>


                    </div>
                </div>
        </div>

        </section>
    </div>
</div>
</div>


<div class="modal fade text-xs-left" id="addVisit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel18"><i class="icon-tree"></i> اضافة مريض</h4>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="form-body">

                        <div class="form-group">
                            <label for="complaintinput3">التاريخ</label>
                            <input type="date" id="complaintinput3" class="form-control" name="date">
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput2">الشكوى</label>
                            <input type="text" id="" class="form-control" placeholder="الشكوى" name="complaint">
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput3">التشخيص</label>
                            <textarea class="form-control" name="" placeholder="التشخيص"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput4">العلاج</label>
                            <input type="email" id="eventRegInput4" class="form-control" placeholder="email"
                                   name="email">
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput5">Contact Number</label>
                            <input type="tel" id="eventRegInput5" class="form-control" name="contact"
                                   placeholder="contact number">
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">اغلاق</button>
                <button type="button" class="btn btn-primary">حفظ</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade text-xs-left" id="addVaccination" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel18"><i class="icon-tree"></i> اضافة طعم</h4>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="form-body">

                        <div class="form-group">
                            <label for="complaintinput3">التاريخ</label>
                            <input type="date" id="complaintinput3" class="form-control" name="date">
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput2"> الطعم</label>
                            <input type="text" id="" class="form-control" placeholder="الطعم" name="vaccination">
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput3"> ملاحظات</label>
                            <textarea class="form-control" name="" placeholder="ملاحظات"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">اغلاق</button>
                <button type="button" class="btn btn-primary">حفظ</button>
            </div>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
