<?php include('../includes/header.php'); ?>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="card">
                <div class="card-header">
                    <h4 class="card-title">  الاعراض   </h4>
                </div>

                <br>
                <br>

                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#defaultSize">
                    اضافة  +
                </button>
                <br>
                <br>
                <div class="table-responsive ">
                    <table class="table mb-0">
                        <thead class="bg-teal bg-lighten-4">
                        <tr>
                            <th>#</th>
                            <th> الاسم </th>
                            <th> عمليان</th>

                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>At Birth: BCG, HB1 </td>
                            <td>
                                <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                </a>
                                <a href="#" class="tb-icon text-warning"> <i class="icon-pencil22  font-large-0"></i>
                                </a>
                                <a href="#" class="tb-icon text-primary"> <i class="icon-eye3  font-large-0"></i>
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <th scope="row">1</th>
                            <td> 1st month: IPV1 </td>
                            <td>
                                <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                </a>
                                <a href="#" class="tb-icon text-warning"> <i class="icon-pencil22  font-large-0"></i>
                                </a>
                                <a href="#" class="tb-icon text-primary"> <i class="icon-eye3  font-large-0"></i>
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>

            </section>
        </div>
    </div>
</div>


<div class="modal fade text-xs-left" id="defaultSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel18"><i class="icon-tree"></i> اضافة  </h4>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="form-body">

                        <div class="form-group">
                            <label for="eventRegInput1"> اسم  </label>
                            <input type="text" id="eventRegInput1" class="form-control" placeholder=" الأسم"
                                   name="fullname">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"> حفظ</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal"> اغلاق</button>
            </div>
        </div>
    </div>
</div>
<?php include('../includes/footer.php'); ?>
