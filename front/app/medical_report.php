<div class="row">
    <form>
        <h4 class="form-section col-md-12"><i class="icon-compose"></i> تقرير طبي</h4>
        <div class="clear"></div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="timesheetinput3"> التاريخ </label>
                <div class="position-relative has-icon-left">
                    <input type="date" id="timesheetinput3" class="form-control input-sm"
                           name="date">
                    <div class="form-control-position">
                        <i class="icon-calendar5"></i>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>المريض</label>
                <input type="text" class="form-control" value="ahmad ahmad">
            </div>
            <div class="form-group">
                <label> الجنس </label>
                <select class="form-control">
                    <option>ذكر</option>
                    <option>انثى</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="طباعة">
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label> البيان</label>
                <textarea class="form-control" rows="10"></textarea>
            </div>

        </div>
    </form>
</div>