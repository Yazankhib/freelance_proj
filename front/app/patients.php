<?php include('includes/header.php'); ?>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="card">


                <br>
                <br>
                <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#defaultSize">
                    اضافة مريض +
                </button>
                <br>
                <br>
                <div class="table-responsive">
                    <table class="table mb-0">
                        <thead class="bg-teal bg-lighten-4">
                        <tr>
                            <th>#</th>
                            <th>الاسم</th>
                            <th>العمر </th>
                            <th>الجنس </th>
                            <th>عرض </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                            <td><a href="<?=('patient.php')?>">  <i class="icon-ios-eye-outline font-large-1"></i> </a></td>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                            <td><a href="<?=('patient.php')?>">  <i class="icon-ios-eye-outline font-large-1"></i> </a></td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Larry</td>
                            <td>the Bird</td>
                            <td>@twitter</td>
                            <td><a href="<?=('patient.php')?>">  <i class="icon-ios-eye-outline font-large-1"></i> </a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
</div>





<div class="modal fade text-xs-left" id="defaultSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel18"><i class="icon-tree"></i> اضافة مريض</h4>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="form-body">

                        <div class="form-group">
                            <label for="eventRegInput1">Full Name</label>
                            <input type="text" id="eventRegInput1" class="form-control" placeholder="name" name="fullname">
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput2">Title</label>
                            <input type="text" id="eventRegInput2" class="form-control" placeholder="title" name="title">
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput3">Company</label>
                            <input type="text" id="eventRegInput3" class="form-control" placeholder="company" name="company">
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput4">Email</label>
                            <input type="email" id="eventRegInput4" class="form-control" placeholder="email" name="email">
                        </div>

                        <div class="form-group">
                            <label for="eventRegInput5">Contact Number</label>
                            <input type="tel" id="eventRegInput5" class="form-control" name="contact" placeholder="contact number">
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" >Save changes</button>
            </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php'); ?>
