<button class="btn btn-outline-warning btn-sm mr-1 mb-1"> <i class="icon-pencil22"></i>  تعديل </button>
<div class="row">
    <div class=" col-md-6">
        <table class="table">
            <thead class="bg-teal bg-lighten-4">
            <tr>
                <th colspan="2"> المعلومات الشخصية</th>
            </tr>
            </thead>
            <tr>
                <td><strong>id_number</strong></td>
                <td> 44455335455
                </td>
            </tr>
            <tr>
                <td><strong>patient_name</strong></td>
                <td>Sheena Shrestha
                </td>
            </tr>
            <tr>
                <td><strong>gender</strong></td>
                <td> Female </td>
            </tr>
            <tr>
                <td><strong>birth_date</strong></td>
                <td> 01/24/1988 </td>
            </tr>
            <tr>
                <td><strong>address</strong></td>
                <td>Kathmandu,Nepal
                </td>
            </tr>
            <tr>
                <td><strong>phone </strong></td>
                <td>
                    <p>555-4567-890</p>
                    <p>555-4567-890</p>
                </td>
            </tr>
            <tr>
                <td><strong>email</strong></td>
                <td>info@support.com</td>
            </tr>
        </table>

    </div>
    <div class=" col-md-6">
        <table class="table">
            <thead class="bg-teal bg-lighten-4">
            <tr>
                <th colspan="2">  معلومات طبية </th>
            </tr>
            </thead>

            <tr>
                <td><strong>insurance</strong></td>
                <td> mmmmmdmdmdm </td>
            </tr>
            <tr>
                <td><strong>insurance_company</strong></td>
                <td> mmmmmm </td>
            </tr>
            <tr>
                <td><strong>general_examination</strong></td>
                <td>mmmmmmmm
                </td>
            </tr>
            <tr>
                <td><strong>labs_examination </strong></td>
                <td>
                    @
                </td>
            </tr>
            <tr>
                <td><strong>blood</strong></td>
                <td>A+</td>
            </tr>
        </table>

    </div>
</div>

<div class="clear"></div>