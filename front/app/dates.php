<?php include('includes/header.php'); ?>


<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section id="description" class="card">
                <div class="card-header">
                    <h4 class="card-title"> المواعيد </h4>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">


                        <div class="table-responsive">
                            <button type="button" class="btn btn-outline-success" data-toggle="modal"
                                    data-target="#addDate">
                                اضافة موعد +
                            </button>

                            <table class="table mb-0">
                                <thead class="bg-teal bg-lighten-4">
                                <tr>
                                    <th>#</th>
                                    <th>التاريخ</th>
                                    <th> اسم المريض</th>
                                    <th> لدية ملف سابق</th>
                                    <th> الطبيب</th>
                                    <th>عمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>01-10-2017</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                    <td>@mdo</td>
                                    <td>
                                        <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                        </a>
                                        <a href="#" class="tb-icon text-warning"> <i
                                                    class="icon-pencil22  font-large-0"></i> </a>
                                        <a href="#" class="tb-icon text-primary"> <i
                                                    class="icon-eye3  font-large-0"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td>01-10-2017</td>
                                    <td>Thornton</td>
                                    <td>@fat</td>
                                    <td>@fat</td>
                                    <td>
                                        <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                        </a>
                                        <a href="#" class="tb-icon text-warning"> <i
                                                    class="icon-pencil22  font-large-0"></i> </a>
                                        <a href="#" class="tb-icon text-primary"> <i
                                                    class="icon-eye3  font-large-0"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td>01-10-2017</td>
                                    <td>the Bird</td>
                                    <td>@twitter</td>
                                    <td>@twitter</td>
                                    <td>
                                        <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                                        </a>
                                        <a href="#" class="tb-icon text-warning"> <i
                                                    class="icon-pencil22  font-large-0"></i> </a>
                                        <a href="#" class="tb-icon text-primary"> <i
                                                    class="icon-eye3  font-large-0"></i>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal fade text-xs-left" id="addDate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel18"><i class="icon-tree"></i> موعد جديد </h4>
            </div>
            <div class="modal-body">
                <form class="form">
                    <div class="form-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="eventRegInput1"> التاريخ  </label>
                                <div class="position-relative has-icon-left">
                                    <input type="date" id="timesheetinput3" class="form-control " name="date">
                                    <div class="form-control-position">
                                        <i class="icon-calendar5"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="eventRegInput1">   الطبيب  </label>
                                <select class="form-control">
                                    <option> select patient name ..  </option>
                                    <option>patient 1  </option>
                                    <option>patient 2  </option>
                                    <option>patient 3  </option>
                                    <option>patient 4  </option>
                                    <option>patient 5  </option>
                                </select>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="eventRegInput1">  المريض  </label>
                                <select class="form-control">
                                    <option> select patient  ..  </option>
                                    <option>patient 1  </option>
                                    <option>patient 2  </option>
                                    <option>patient 3  </option>
                                    <option>patient 4  </option>
                                    <option>patient 5  </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="eventRegInput1"> لدية ملف سابق  </label>
                                 <div class="clear"></div>
                                <div class="input-group">
                                    <label class="display-inline-block custom-control custom-radio ml-1">
                                        <input type="radio" name="customer1" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description ml-0">Yes</span>
                                    </label>
                                    <label class="display-inline-block custom-control custom-radio">
                                        <input type="radio" name="customer1" checked="" class="custom-control-input">
                                        <span class="custom-control-indicator"></span>
                                        <span class="custom-control-description ml-0">No</span>
                                    </label>
                                </div>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary"> حفظ</button>
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal"> اغلاق</button>
            </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php'); ?>
