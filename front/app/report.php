<div class="row">

    <div class="section-head">
        تقرير المريض

    </div>

    <div class="clear"></div>

    <div class="col-md-12">
        <table class="table table-striped table-responsive">

            <thead class="bg-teal bg-lighten-4">
            <tr>
                <td colspan="4" class="header1">المعلومات الشخصية</td>
            </tr>
            </thead>
            <tr>
                <td class=" " width=""><strong>الرقم</strong></td>
                <td class=" ">3&;</td>
                <td class=" "><strong>الأسم</strong></td>
                <td class=" ">Frank Sinatra&nbsp;</td>

            </tr>

            <tr>
                <td class=" "><strong>تاريخ الميلاد</strong></td>
                <td class=" ">2016-11-12&nbsp;&nbsp;&nbsp; العمر 0Y 10M 29d</td>
                <td class=" "><strong>الجنس</strong></td>
                <td class=" ">ذكر&nbsp;</td>

            </tr>

            <tr>
                <td class=" "><strong>العنوان</strong></td>
                <td class=" "> رام الله &nbsp;</td>
                <td class=" "><strong>رقم الهاتف #2</strong>
                </td>
                <td class=" ">090908076;</td>

            </tr>
            <tr>
                <td class=" "><strong>رقم الهاتف #1</strong></td>
                <td class=" ">0599112233&nbsp;</td>
                <td class=" "><strong>البريد الإلكتروني</strong></td>
                <td class=" ">toto@tata.com&nbsp;</td>

            </tr>

            <tr>
                <td class=" "><strong>ملاحظات</strong></td>
                <td class=" " colspan="3">non&nbsp;</td>
            </tr>
        </table>

    </div>


    <div class="clear"></div>

    <div class="section-head">
        معلومات الزيارات

    </div>

    <div class="col-md-6">
        <table class="table table-striped">

            <thead class="bg-teal bg-lighten-4 ">
            <tr>
                <td class="header1"> <strong>رقم الزيارة</strong></td>
                <td class="header1">1</td>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="header2"> <strong>التاريخ</strong></td>
                <td class="header2">2016-12-05</td>
            </tr>
            <tr>
                <td class="header2"><strong>الطبيب المعالج</strong></td>
                <td class="header2"> DR Ahmad</td>
            </tr>
            <tr>
                <td class=" "><strong>علامات الحياة</strong></td>
                <td class=" " dir="ltr">P: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TEM: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BP:
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WT: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RR: &nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td class=" "><strong>العلاج المقترح</strong></td>
                <td class=" ">ANAFRANIL 25MG TABLET tablet sugar coated&nbsp;</td>
            </tr>
            <tr>
                <td class=" "><strong>ساعة الحضور</strong></td>
                <td class=" ">13:34:00&nbsp;</td>
            </tr>
            <tr>
                <td class=" "><strong>ساعة المغادرة</strong></td>
                <td class=" ">15:40:28;</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="col-md-6">
        <table class="table table-striped">

            <thead class="bg-teal bg-lighten-4 ">
            <tr>
                <td class="header1"> رقم الزيارة</td>
                <td class="header1">2</td>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="header2"> التاريخ</td>
                <td class="header2">2016-12-05</td>
            </tr>
            <tr>
                <td class="header2">الطبيب المعالج</td>
                <td class="header2"> DR Ahmad</td>
            </tr>
            <tr>
                <td class=" ">علامات الحياة</td>
                <td class=" " dir="ltr">P: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TEM: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BP:
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WT: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RR: &nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td class=" ">العلاج المقترح</td>
                <td class=" " dir="ltr">ANAFRANIL 25MG TABLET tablet sugar coated&nbsp;</td>
            </tr>
            <tr>
                <td class=" ">ساعة الحضور</td>
                <td class=" ">13:34:00&nbsp;</td>
            </tr>
            <tr>
                <td class=" ">ساعة المغادرة</td>
                <td class=" ">15:40:28;</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table table-striped">

            <thead class="bg-teal bg-lighten-4 ">
            <tr>
                <td class="header1"> رقم الزيارة</td>
                <td class="header1">3</td>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="header2"> التاريخ</td>
                <td class="header2">2016-12-05</td>
            </tr>
            <tr>
                <td class="header2">الطبيب المعالج</td>
                <td class="header2"> DR Ahmad</td>
            </tr>
            <tr>
                <td class=" ">علامات الحياة</td>
                <td class=" " dir="ltr">P: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TEM: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BP:
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WT: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RR: &nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td class=" ">العلاج المقترح</td>
                <td class=" " dir="ltr">ANAFRANIL 25MG TABLET tablet sugar coated&nbsp;</td>
            </tr>
            <tr>
                <td class=" ">ساعة الحضور</td>
                <td class=" ">13:34:00&nbsp;</td>
            </tr>
            <tr>
                <td class=" ">ساعة المغادرة</td>
                <td class=" ">15:40:28;</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="col-md-6">
        <table class="table table-striped">

            <thead class="bg-teal bg-lighten-4 ">
            <tr>
                <td class="header1"> رقم الزيارة</td>
                <td class="header1">4</td>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="header2"> التاريخ</td>
                <td class="header2">2016-12-05</td>
            </tr>
            <tr>
                <td class="header2">الطبيب المعالج</td>
                <td class="header2"> DR Ahmad</td>
            </tr>
            <tr>
                <td class=" ">علامات الحياة</td>
                <td class=" " dir="ltr">P: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TEM: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BP:
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WT: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RR: &nbsp;&nbsp;</td>
            </tr>
            <tr>
                <td class=" ">العلاج المقترح</td>
                <td class=" " dir="ltr">ANAFRANIL 25MG TABLET tablet sugar coated&nbsp;</td>
            </tr>
            <tr>
                <td class=" ">ساعة الحضور</td>
                <td class=" ">13:34:00&nbsp;</td>
            </tr>
            <tr>
                <td class=" ">ساعة المغادرة</td>
                <td class=" ">15:40:28;</td>
            </tr>
            </tbody>
        </table>
    </div>

</div>