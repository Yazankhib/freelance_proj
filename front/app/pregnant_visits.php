<div class="table-responsive">


    <a  href="new_pregnant_visit.php"  class=" button btn btn-outline-success">
        اضافة زيارة +
    </a>

    <table class="table mb-0">
        <thead class="bg-teal bg-lighten-4">
        <tr>
            <th>#</th>
            <th>التاريخ</th>
            <th>الشكوى</th>
            <th>التشخيص</th>
            <th>العلاج</th>
            <th>عمليات</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th scope="row">1</th>
            <td>01-10-2017</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td>@mdo</td>
            <td>
                <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                </a>
                <a href="#" class="tb-icon text-warning"> <i
                        class="icon-pencil22  font-large-0"></i> </a>
                <a href="#" class="tb-icon text-primary"> <i
                        class="icon-eye3  font-large-0"></i> </a>
            </td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>01-10-2017</td>
            <td>Thornton</td>
            <td>@fat</td>
            <td>@fat</td>
            <td>
                <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                </a>
                <a href="#" class="tb-icon text-warning"> <i
                        class="icon-pencil22  font-large-0"></i> </a>
                <a href="#" class="tb-icon text-primary"> <i
                        class="icon-eye3  font-large-0"></i> </a>
            </td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>01-10-2017</td>
            <td>the Bird</td>
            <td>@twitter</td>
            <td>@twitter</td>
            <td>
                <a href="#" class="tb-icon text-danger"> <i class="icon-bin font-large-0"></i>
                </a>
                <a href="#" class="tb-icon text-warning"> <i
                        class="icon-pencil22  font-large-0"></i> </a>
                <a href="#" class="tb-icon text-primary"> <i
                        class="icon-eye3  font-large-0"></i> </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>