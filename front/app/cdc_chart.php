<?php include('includes/header.php'); ?>

<div class="app-content content container-fluid">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section class="card">
                <div class="card-block chartjs">
                    <canvas id="line-chart" height="500"></canvas>
                </div>
            </section>
        </div>
    </div>
</div>






<?php include('includes/footer.php'); ?>

<script src="<?=base_url?>/app-assets/js/scripts/charts/chartjs/line/line.js" type="text/javascript"></script>
