<?php


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
        'user_name' => $faker->name,
        'clinic_address' => $faker->address,
        'home_address' => $faker->address,
        'clinic_phone' => $faker->phonenumber,
        'home_phone' => $faker->phonenumber,
        'mobile1' => $faker->phonenumber,
        'mobile2' => $faker->phonenumber,
        'specialization' => $faker->realText(103),
        'email' => $faker->unique()->email,
        'password' => $password ?: $password = bcrypt('secret'),
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
        'group_id' => function () {
            return factory(App\Groups::class)->create()->id;
        }, 
    ];
});
$factory->define(App\Groups::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\Clinics::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\Labs::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Clinicalexaminations::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'type' => $faker->name,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Birthtypes::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Xrays::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Patients::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'patient_name' => $faker->name,
        'gender' => $faker->boolean,
        'birth_date' => $faker->date,
        'address' => $faker->address,
        'phone1' => $faker->phonenumber,
        'phone2' => $faker->phonenumber,
        'email' => $faker->unique()->email,
        'id_number' => $faker->creditCardNumber,
        'is_insurance' =>$faker->boolean,
        'insurance_company' => $faker->company,
        'blood' => $faker->name,
        'sensitivity' => $faker->name,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
        'general_examination' => function () {
            return factory(App\Clinicalexaminations::class)->create()->id;
        },
        'labs_examination' => function () {
            return factory(App\Labs::class)->create()->id;
        },
    ];
}); 

$factory->define(App\Clinicalexamdetails::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
        'clinicalexam_id' => function () {
            return factory(App\Clinicalexaminations::class)->create()->id;
        },

    ];
}); 

$factory->define(App\Births::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'birth_no' => $faker->randomDigit,
        'birth_date' => $faker->date,
        'birth_gender' =>$faker->boolean,
        'birth_weeks' => $faker->randomDigit,
        'child_wh' => $faker->randomFloat,
        'note' => $faker->paragraph,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
        'birth_type_id' => function () {
            return factory(App\Birthtypes::class)->create()->id;
        },
        'patient_id' => function () {
            return factory(App\Patients::class)->create()->id;
        },
    ];
}); 

$factory->define(App\Visits::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'revisit_time' => $faker->time,
        'complaints' => $faker->paragraph,
        'symptoms' => $faker->paragraph,
        'diagnosis' => $faker->paragraph,
        'recom_treat' => $faker->paragraph,
        'final_diagnosis' => $faker->paragraph,
        'visit_date' => $faker->date,
        'revisit_date' => $faker->date,
        'ask_for_help' =>$faker->boolean,
        'active' => "1",
        'age' => $faker->company,
        'in_time' => $faker->time,
        'out_time' => $faker->time,
        'weight' => $faker->randomFloat,
        'note' => $faker->paragraph,
        'item_p' => $faker->paragraph,
        'item_tem' => $faker->paragraph,
        'item_bp' => $faker->paragraph,
        'item_wt' => $faker->paragraph,
        'item_rr' => $faker->paragraph,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
        'patient_id' => function () {
            return factory(App\Patients::class)->create()->id;
        },
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        },
    ];
}); 

$factory->define(App\Cdc::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'cdc_date' => $faker->date,
        'weight' =>rand(1,100),
        'length' => rand(1,100),
        'head' =>rand(1,100),
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
        'births_id' => function () {
            return factory(App\Births::class)->create()->id;
        },
        'visit_id' => function () {
            return factory(App\Visits::class)->create()->id;
        },
    ];
}); 

$factory->define(App\Childmids::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Cities::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->country,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Complaints::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->paragraph,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Expenses::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'expenses_name' => $faker->name,
        'expense_date' => $faker->date,
        'value' =>$faker->randomFloat,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Medicines::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
        'active_name' => $faker->name,
        'company' => $faker->company,
        'number' => rand(1,100000000000),
        'amount' => $faker->paragraph,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Midtimes::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
        'mid_no' => $faker->randomDigit,
        'mid_date' => $faker->date,
        'note' => $faker->paragraph,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        },
        'patient_id' => function () {
            return factory(App\Patients::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Payments::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->name,
        'amount' =>  rand(1,100),
        'payment_date' => $faker->date,
        'payment' => $faker->randomFloat,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        },
        'visit_id' => function () {
            return factory(App\Visits::class)->create()->id;
        }, 
    ];
});

$factory->define(App\Symptoms::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->paragraph,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});

$factory->define(App\diagnoses::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ar_SA');
    return [
        'name' => $faker->paragraph,
        'clinic_id' => function () {
            return factory(App\Clinics::class)->create()->id;
        }, 
    ];
});
