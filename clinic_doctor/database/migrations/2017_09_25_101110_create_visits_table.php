<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('patient_id');
            $table->date('visit_date');
            $table->date('revisit_date');
            $table->time('revisit_time');
            $table->time('in_time');
            $table->time('out_time');
            $table->boolean('active');
            $table->string('complaints')->nullable();
            $table->string('symptoms')->nullable();
            $table->string('diagnosis')->nullable();
            $table->string('recom_treat')->nullable();
            $table->string('note')->nullable();
            $table->string('ask_for_help')->nullable();
            $table->string('final_diagnosis')->nullable();
            $table->string('age')->nullable();
            $table->string('weight')->nullable();
            $table->string('item_p')->nullable();
            $table->string('item_tem')->nullable();
            $table->string('item_bp')->nullable();
            $table->string('item_wt')->nullable();
            $table->string('item_rr')->nullable();
            $table->boolean('p1n0');
            $table->string('embryoweeknumbers')->nullable();
            $table->string('embryostate')->nullable();
            $table->string('uterusheight')->nullable();
            $table->string('embryopulse')->nullable();
            $table->string('swelling')->nullable();
            $table->string('weightpreg')->nullable();
            $table->string('pressure')->nullable();
            $table->string('ALBtest')->nullable();
            $table->string('Gluctest')->nullable();
            $table->string('BPD')->nullable();
            $table->string('FL')->nullable();
            $table->string('AC')->nullable();
            $table->string('EFbwt')->nullable();
            $table->string('Placenta')->nullable();
            $table->string('AF')->nullable();
            $table->string('Abnormalities')->nullable();
            $table->string('embryoweight')->nullable();
            $table->string('embryoheight')->nullable();
            $table->string('embryoheadcircumference')->nullable();
            $table->unsignedBigInteger('clinic_id');
            $table->timestamps();
        });

        Schema::table('visits', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');//malak: from cities table
            $table->foreign('patient_id')->references('id')->on('patients');//malak: from cities table
            $table->foreign('user_id')->references('id')->on('users');//malak: from cities table

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
