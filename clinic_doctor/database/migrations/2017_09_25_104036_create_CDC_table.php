<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCDCTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('cdcs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('births_id');
            $table->float('weight');
            $table->date('cdc_date');
            $table->float('length');
            $table->float('head');
            $table->unsignedBigInteger('visit_id');
            $table->unsignedBigInteger('clinic_id');
            $table->timestamps();
        });
        
        Schema::table('cdcs', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');
            $table->foreign('births_id')->references('id')->on('births');
            $table->foreign('visit_id')->references('id')->on('visits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cdcs');
    }
}
