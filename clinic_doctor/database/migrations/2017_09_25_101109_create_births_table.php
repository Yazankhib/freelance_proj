<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBirthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('births', function (Blueprint $table) {
              $table->bigIncrements('id');
            $table->unsignedBigInteger('patient_id');
            $table->integer('birth_no');
            $table->date('birth_date');
            $table->unsignedBigInteger('birth_type_id');
            $table->integer('birth_gender');
            $table->integer('birth_weeks');
            $table->float('child_wh');
            $table->string('note');
            $table->unsignedBigInteger('clinic_id');
            $table->timestamps();
        });
           Schema::table('births', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');
            $table->foreign('patient_id')->references('id')->on('patients');
            $table->foreign('birth_type_id')->references('id')->on('birth_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('births');
    }
}
