<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicalexaminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('clinicalexaminations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->nullable();
            $table->boolean('active');
            $table->unsignedBigInteger('clinic_id');
            $table->timestamps();
        });

        Schema::table('clinicalexaminations', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');//malak: from cities table
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinicalexaminations');
    }
}
