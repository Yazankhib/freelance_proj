<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(500);
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('patient_name');
            $table->string('gender')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('address')->nullable();
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('email')->nullable();
            $table->string('id_number')->unique();
            $table->integer('is_insurance')->nullable();
            $table->string('insurance_company')->nullable();
            $table->string('general_examination')->nullable();
            $table->string('labs_examination')->nullable();
            $table->string('blood')->nullable();
            $table->string('sensitivity')->nullable();
            $table->unsignedBigInteger('clinic_id');
            $table->timestamps();
        });

        Schema::table('patients', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');//malak: from cities table

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
