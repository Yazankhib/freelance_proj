<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsRightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('GroupsRights', function (Blueprint $table) {
             $table->unsignedBigInteger('group_id');
            $table->unsignedBigInteger('right_id');
            $table->primary(['group_id','right_id']);
            $table->unsignedBigInteger('clinic_id');
            $table->timestamps();
        });
        Schema::table('GroupsRights', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');//malak: from cities table
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('GroupsRights');
    }
}
