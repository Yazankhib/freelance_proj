<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitraysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('visitrays', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('visit_id');
            $table->integer('ray_id');
            $table->unsignedBigInteger('clinic_id');
            $table->boolean('active');
            $table->rememberToken();
            $table->primary(['id', 'visit_id', 'ray_id']);
            $table->timestamps();
        });
    
        Schema::table('visitrays', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');//malak: from cities table
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitrays');
    }
}
