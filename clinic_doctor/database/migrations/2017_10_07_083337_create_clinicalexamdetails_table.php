<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClinicalexamdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('clinicalexamdetails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->unsignedBigInteger('clinic_id');
            $table->boolean('active');
            $table->unsignedBigInteger('clinicalexam_id');
            $table->timestamps();
        });
        Schema::table('clinicalexamdetails', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');
            $table->foreign('clinicalexam_id')->references('id')->on('clinicalexaminations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinicalexamdetails');
    }
}
