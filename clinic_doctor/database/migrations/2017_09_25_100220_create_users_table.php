<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::defaultStringLength(191);
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('user_name');
            $table->string('clinic_address')->nullable();
            $table->string('home_address')->nullable();
            $table->string('email')->unique();
            $table->string('clinic_phone')->nullable();
            $table->string('home_phone')->nullable();
            $table->string('mobile1')->nullable();
            $table->string('mobile2')->nullable();
            $table->string('specialization')->nullable();
            $table->unsignedBigInteger('clinic_id');
            $table->unsignedBigInteger('group_id');
            $table->string('password');
            $table->boolean('active');
            $table->rememberToken();
            $table->timestamps();
        });
        
        Schema::table('users', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');//malak: from cities table
            $table->foreign('group_id')->references('id')->on('Groups');//malak: from cities table
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
