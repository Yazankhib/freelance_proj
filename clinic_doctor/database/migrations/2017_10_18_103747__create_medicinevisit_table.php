<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicinevisitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('medicinevisits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('visit_id');
            $table->integer('medicine_id');
            $table->unsignedBigInteger('clinic_id');
            $table->boolean('active');
            $table->rememberToken();
            $table->primary(['id', 'visit_id', 'medicine_id']);
            $table->timestamps();
        });
        Schema::table('medicinevisits', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');//malak: from cities table
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicinevisits');
    }
}
