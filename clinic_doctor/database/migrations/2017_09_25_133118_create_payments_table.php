<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->boolean('is_insurance')->nullable();
            $table->string('insurance_company')->nullable();
            $table->float('amount')->nullable();
            $table->float('payment')->nullable();
            $table->float('labspayment')->nullable();
            $table->float('testpayment')->nullable();
            $table->float('anotherpayment')->nullable();
            $table->boolean('active');
            $table->unsignedBigInteger('clinic_id')->nullable();
            $table->unsignedBigInteger('visit_id')->nullable();
            $table->timestamps();
        });
        Schema::table('payments', function($table) {
            $table->foreign('clinic_id')->references('id')->on('clinics');//malak: from cities table
            $table->foreign('visit_id')->references('id')->on('visits');//malak: from cities table

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
