<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Midtimes extends Model
{
    protected $table = 'midtimes';
    protected $fillable = ['id', 'patient_id', 'vaccinate_id', 'mid_date', 'note', 'active', 'clinic_id'];
}
