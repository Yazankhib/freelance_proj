<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicinevisit extends Model
{
    protected $table = 'medicinevisits';
    protected $fillable = ['id', 'medicine_id', 'visit_id', 'active','clinic_id'];
}
