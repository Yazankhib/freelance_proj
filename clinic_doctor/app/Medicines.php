<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicines extends Model
{
    protected $table = 'medicines';
    protected $fillable = ['id', 'name', 'active_name', 'active', 'company', 'number', 'amount', 'clinic_id'];
}
