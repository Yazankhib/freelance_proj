<?php

namespace App\Policies;

use App\User;
use App\Visits;
use Illuminate\Auth\Access\HandlesAuthorization;

class VisitsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the visits.
     *
     * @param  \App\User  $user
     * @param  \App\Visits  $visits
     * @return mixed
     */
    public function view(User $user, Visits $visits)
    {
        //
    }

    /**
     * Determine whether the user can create visits.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the visits.
     *
     * @param  \App\User  $user
     * @param  \App\Visits  $visits
     * @return mixed
     */
    public function update(User $user)
    {
        //
        $bool= false ;
         $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
            if ($rgiht->name =='EditVisitOfPatient')
            {
                $bool = true;
                
            }
        }
        if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }     
        
    }

    /**
     * Determine whether the user can delete the visits.
     *
     * @param  \App\User  $user
     * @param  \App\Visits  $visits
     * @return mixed
     */
    public function delete(User $user, Visits $visits)
    {
        //
    }

    /********* these Functions for  Visits with type Pregnant Visits and in this release postpone ************/
    /*
    public function ViewVisitPregnantOfPatient(User $user)
    {
        //
        $bool= false ;
         $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
            if ($rgiht->name =='ViewPregnantVisit')
            {
                $bool = true;
                
            }
        }
        if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }     
        
    }*/
    

}
