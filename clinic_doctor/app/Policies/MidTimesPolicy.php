<?php

namespace App\Policies;

use App\User;
use App\Midtimes;
use Illuminate\Auth\Access\HandlesAuthorization;

class MidTimesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the midtimes.
     *
     * @param  \App\User  $user
     * @param  \App\Midtimes  $midtimes
     * @return mixed
     */
    public function view(User $user)
    {
        //
      /*  if ($user->getPermisions() == 'ViewMidtimesDetails' )
        {
            return true ;
        }
        else {
            return false ;
        }*/
        $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='ViewMidtimesDetails')
            {
                 $bool = true;
            }
        }
        if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
    
    }

    /**
     * Determine whether the user can create midtimes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        $NameRight = $user->getPermisions();
         /*if ($user->getPermisions() == 'InsertMidtimesDetails' )
        {
            return true ;
        }
        else {
            return false ;
        }
        */
        $bool= false ;
        foreach($NameRight as $rgiht){
            if ($rgiht->name =='InsertMidtimesDetails')
            {
                $bool = true;
                //return true; 
            }
            /*else 
            {
                return false ;
            }*/
        }
        if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }       

    }

    /**
     * Determine whether the user can update the midtimes.
     *
     * @param  \App\User  $user
     * @param  \App\Midtimes  $midtimes
     * @return mixed
     */
    public function update(User $user, Midtimes $midtimes)
    {
        //
    }

    /**
     * Determine whether the user can delete the midtimes.
     *
     * @param  \App\User  $user
     * @param  \App\Midtimes  $midtimes
     * @return mixed
     */
    public function delete(User $user, Midtimes $midtimes)
    {
        //
    }
  
}
