<?php

namespace App\Policies;

use App\User;
use App\Patients;
use Illuminate\Auth\Access\HandlesAuthorization;

class PatientsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the patients.
     *
     * @param  \App\User  $user
     * @param  \App\Patients  $patients
     * @return mixed
     */
    public function view(User $user, Patients $patients)
    {
        //
    }

    /**
     * Determine whether the user can create patients.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='addPatients')
            {
                 $bool = true;
            }
        }
         if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
   

    }

    /**
     * Determine whether the user can update the patients.
     *
     * @param  \App\User  $user
     * @param  \App\Patients  $patients
     * @return mixed
     */
    public function update(User $user)
    {
        //
        $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='EditPatientDetails')
            {
                 $bool = true;
            }
        }
         if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
   
    }

    /**
     * Determine whether the user can delete the patients.
     *
     * @param  \App\User  $user
     * @param  \App\Patients  $patients
     * @return mixed
     */
    public function delete(User $user, Patients $patients)
    {
        //
    }
    public function test ($user) 
    {
      
        if ($user->getPermisions() == 'addPatients' )
        {
            return true ;
        }
        else {
            return false ;
        }
        /*
         if ($user->group_id== 1)//'bshami@gmail.com')
             {
                return true;
             }
             return false;
             */
    }

    /******** report Of patient //report.vue component************/
    public function ViewReportOfPatient (User $user) 
    {
      
         $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='ViewReportPatient')
            {
                 $bool = true;
            }
      }  
      if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
    
    }

    /********Patient Visists **************/ 
     public function ViewVisitOfPatient (User $user) 
    {
      
         $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='ViewVisitPatient')
            {
                 $bool = true;
            }
        }
        if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
    
    }
    
    /******** add New Patient Visit **************/
     public function addNewVisitOfPatient (User $user) 
    {
      
         $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='AddNewVisitPatient')
            {
                 $bool = true;
            }
        }
        if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
    
    }

    //*********this for Insert Medical report*************///
         public function AddMedicalReportForPatient (User $user) 
    {
      
         $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='AddMedicalReport')
            {
                 $bool = true;
            }
        }
        if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
    
    }

     //*********this role for Difintions Page *************///
         public function Dinistions_role (User $user) 
    {
      
         $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='Definitions')
            {
                 $bool = true;
            }
        }
        if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
    
    }
    

}
