<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UsersPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='InsertUsersInClinic')
            {
                 $bool = true;
            }
        }
         if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
   
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        //
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        //
    }

    /// ****this For Super Admin Role "System owner" to add clinics and admin user for it ***** ///
    public function AdminInsertUsers(User $user)
    {
        //
        $bool= false ;
        $NameRight = $user->getPermisions();
        foreach($NameRight as $rgiht){
        if ($rgiht->name =='SuperAdmin')
            {
                 $bool = true;
            }
        }
         if ($bool)
        {
            return true ;
        }
        else 
        {
            return false ;
        }
    }
}
