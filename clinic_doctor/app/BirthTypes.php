<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BirthTypes extends Model
{
    protected $table = 'birth_types';
    protected $fillable = ['id', 'name', 'active', 'clinic_id'];
}
