<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitrays extends Model
{
    protected $table = 'visitrays';
    protected $fillable = ['id', 'ray_id', 'visit_id', 'active', 'clinic_id'];
}
