<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password','user_name','active','clinic_address','home_address','clinic_phone','home_phone','mobile1','mobile2','specialization','clinic_id','group_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
      public function groupes()
    {
        return $this->belongsToMany(Groupes::class);
    }
    public function getPermisions() {
         $rightname = "";
        $d = $this->group_id ;
        $group_id = Groups::find($d);
         $resultr = Groups::select('name')->where('id',$d)->first();
         $rights= GroupsRights::select('Right_id')->where('group_id',$d)->get();
        foreach ($rights as $right) {
        $result = Rights::select('name')->where('id',$right->Right_id)->first();
        $rightname[] = $result;
            
        //$resultr->name ;
        /*
   $rights = select rights from group_right 
         where group_id = $grroup_id 
         select name from rights 
         where right = $rights 
*/
    }
    return  $rightname;
    }

}
