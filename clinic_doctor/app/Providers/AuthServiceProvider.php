<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Midtimes' => 'App\Policies\MidTimesPolicy',
        'App\Patients' => 'App\Policies\PatientsPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('test','App\Policies\PatientsPolicy@test'); 
         Gate::define('InsertPatient','App\Policies\PatientsPolicy@create'); 
         Gate::define('EditPatientDetails','App\Policies\PatientsPolicy@update'); 
        Gate::define('ViewMidtimesDetails','App\Policies\MidTimesPolicy@view'); // view Midtime Permision
        Gate::define('InsertMidtimesDetails','App\Policies\MidTimesPolicy@create'); // insert/delete on Midtime Permision
        //Gate::define('ViewReportPatient','App\Policies\PatientsPolicy@ViewReportOfPatient'); // this for both view patient and View Report Patient // report.vue component// patient.vue 
        Gate::define('ViewVisitPatient','App\Policies\PatientsPolicy@ViewVisitOfPatient'); // this for both view patient Visit and View Report Patient // report.vue component// patientinfo.vue 
        Gate::define('addNewVisitPatient','App\Policies\PatientsPolicy@addNewVisitOfPatient'); // this for in add new visit on  newvisit.vue  component
        Gate::define('EditVisitPatient','App\Policies\VisitsPolicy@update'); // this for in Edit visit on  editvisit.vue  component // it will limit insert too on ( payment,clinicalexaminationsvisits,examinationdetailsvisits,visitlabs,visitrays)
        Gate::define('AddMedicalReport','App\Policies\PatientsPolicy@AddMedicalReportForPatient'); // this for insert Medical report // medical_report.vue component 
        Gate::define('Definitions','App\Policies\PatientsPolicy@Dinistions_role'); // this for insert Medical report // medical_report.vue component 
        Gate::define('InsertUsersInClinic','App\Policies\UsersPolicy@create'); // this role for clinic Admin to  insert users with roles in his clinic // register.vue component      
        Gate::define('InsertUsersForAdmin','App\Policies\UsersPolicy@AdminInsertUsers'); // this role for clinic Admin to  insert users with roles in his clinic // register.vue component      

       // Gate::define('ViewVisitPregnantPatient','App\Policies\VisitsPolicy@ViewVisitPregnantOfPatient'); // this for view Pregnant Visit // pregnant_visit.vue 

        /* function ($user) {
       
             if ($user->group_id== 1)//'bshami@gmail.com')
             {
                return true;
             }
             return false;
        });
        */
        Passport::routes();
        //
    }
}
