<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examinationdetails_visits extends Model
{
    protected $table = 'examinationdetails_visits';
    protected $fillable = ['id', 'exdetail_id', 'visit_id', 'active', 'clinic_id'];
}
