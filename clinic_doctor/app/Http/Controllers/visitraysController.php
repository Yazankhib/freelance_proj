<?php

namespace App\Http\Controllers;

use App\Xrays;
use App\Visitrays;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
class visitraysController extends Controller
{
	 /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        //malak: unused
       /* $visitrays = Visitrays::all();
        if($visitrays->isEmpty()){
            return 'no rays found !!';
        } else {
            return $visitrays;
        }*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//malak: add ray to visit
    {
        $userr = $this->request->user();
        $test ="exist";
        $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        for($i =0; $i<count($request->input('ray_id')); $i++){
        $visitrays = Visitrays::where('visit_id', $request->input('visit_id'))->where('clinic_id' , $userr->clinic_id)->where('ray_id', $request->input('ray_id')[$i])->get();
        if($visitrays->isEmpty())
        {
        $visitrays = Visitrays::create(array(
            'ray_id' => $request->input('ray_id')[$i],
            'visit_id' => $request->input('visit_id'),
            'clinic_id' => $userr->clinic_id,
            'active' => '1',
                 ));
        if($visitrays->save()){
            $visitrays = Visitrays::where('clinic_id' , $userr->clinic_id);
            //return $visitrays;
        } else {
            return "unsuccessfully added ray test";
        }
        } else {
            return $test;
        }
    }
        }
        else {
            return "you are not authorized to add on visit rays";
        }
}
/**
     * Display the specified resource.
     *
     * @param  \App\Visitrays  $visitrays
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)//malak: show all rays of visit
    {
        $userr = $this->request->user();
        $rayname = "";
        $visitrays = Visitrays::where('visit_id', $id)->where('clinic_id' , $userr->clinic_id)->get();
        if(!$visitrays->isEmpty()){

        foreach ($visitrays as $visitray) {
            $rayn = Xrays::where('id', $visitray->ray_id)->where('clinic_id' , $userr->clinic_id)->get();
            if(!$rayn->isEmpty()){
                $rayname[] = $rayn;
            }
        }
        return $rayname;
    }
    else {
        return "no tests ray";
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visitrays  $visitrays
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visitrays  $visitrays
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visitrays  $visitrays
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }

    public function delete_ray_visit($visitid, $rayid){

        $visitrays = Visitrays::where('visit_id', $visitid)->where('ray_id', $rayid)->first();
        if($visitrays){
        $visitrays->delete($visitid, $rayid);
        } else {
            return "notexist";
        }
    }
}
