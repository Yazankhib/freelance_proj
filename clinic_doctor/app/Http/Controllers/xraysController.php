<?php

namespace App\Http\Controllers;

use App\Xrays;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;

class xraysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        $user_clinic = $this->request->user();
        $Xrays_info = Xrays::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
        if($Xrays_info->isEmpty()){
            return 'no Xrays found !!';
        } else {
            return $Xrays_info;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $user_clinic = $this->request->user();
        $test ="exist";
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $xrays = Xrays::where('name', $request->input('name'))->where('active',1)->where('clinic_id',$user_clinic->clinic_id)->get();
        if($xrays->isEmpty()){
        $xrays = Xrays::create(array(
                'name' => $request->input('name'),
                'clinic_id' => $user_clinic->clinic_id,
                'active' => '1',
                ));
        if($xrays->save()){
            $user_clinic = $this->request->user();
            $xrays = Xrays::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            //$xrays = Xrays::all();
            return $xrays;
        } else {
            return "unsuccessfully added Xrays :(";
        }
    } else {
        return $test;
    }     
    }
    else {
        return "you are not authorized to add on rays";
    }
    
    }/**
     * Display the specified resource.
     *
     * @param  \App\Xrays  $xrays
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $xrays = Xrays::where('active',1)->find($id);
        if($xrays){
            return $xrays;
        } else {
            return "not found";
        }
    }
    else {
        return "you are not authorized to show the rays";
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Xrays  $xrays
     * @return \Illuminate\Http\Response
     */
    public function edit(Xrays $xrays)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Xrays  $xrays
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_clinic = $this->request->user();
        $test ="noth";
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $xrays = Xrays::where('active',1)->find($id);
      //  $allxrays = Xrays::all();
        $allxrays = Xrays::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();

        if($request->input('editname') == $xrays->name){
            return $test;
        }
        else {
            foreach($allxrays as $allxray){
                if($allxray->name == $request->input('editname')){
                    return "exist";
                } else {
                    $test ="ss";
                }
            }
            if($test == "ss"){
                $xrays->name = $request->input('editname');
                $xrays->clinic_id = $user_clinic->clinic_id;
                $xrays->active = '1';
                $xrays->save();
                //$xrays = Xrays::all();
                $xrays = Xrays::where('clinic_id',$user_clinic->clinic_id)->where('active', 1)->get();
                return $xrays;
            }
        }
    }
    else {
        return "you are not authorized to edit on rays";
    }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Xrays  $xrays
     * @return \Illuminate\Http\Response
     */
    public function destroy(Xrays $xrays, $id)
    {
        $user_clinic = $this->request->user();
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $xrays = Xrays::where('active',1)->find($id);
        if(!$xrays){
            return "can not find lab";
        } else {
            $xrays->active = '0';
            $xrays->save();
            $xrays = Xrays::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            return $xrays;
        }
    }
    else {
                return "you are not authorized to  delete Xrays";
    }
    }
}
