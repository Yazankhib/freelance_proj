<?php

namespace App\Http\Controllers;

use App\Visits;
use App\Patients;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
class patientsController  extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        protected $request;
        public function __construct(Request $request) {
        $this->request = $request;
        }

    public function index()
    {
        $userr = $this->request->user();
        //$user_clinic =user::where('clinic_id',$user->clinic_id)->first();
         
        $patients = Patients::where('clinic_id',$userr->clinic_id)->get();
        if($patients->isEmpty()){
            return 'no patients found !!';
        } else {
            return $patients;
        }
        //$user_clinic = DB::table('users')->get();
       /* foreach ($user_clinic as $clinic){
            $sql="select * from users where id=";
            $user_clinic_info = DB::select($sql);
            //dd($dealers_info);
            //return $dealers_info;
        }*/
        // $user_clinic'';
        /*
        $patients = Patients::all();
        if($patients->isEmpty()){
            return 'no patients found !!';
        } else {
            return $patients;
        }
        */
        
       
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*        $user = Auth::guard('api');
         if (Gate::allows('InsertPatient', $user)){
        return 'obada Success';
    }else {
        return 'Fails not authorized';//'you are not authorized';
    }*/
        $userr = $this->request->user();
        $test ="exist";
        // for authorization insert on Patient Table 
        $user = Auth::guard('api');
        if (Gate::allows('InsertPatient', $user)) {
        $patient = Patients::where('id_number', $request->input('id_number'))->where('clinic_id', $userr->clinic_id)->get();
        if($patient->isEmpty()){
            $patients = Patients::create(array(
                'patient_name' => $request->input('patient_name'),
                'gender' => $request->input('gender'),
                'birth_date' => $request->input('birth_date'),
                'address' => $request->input('address'),
                'phone1' => $request->input('phone1'),
                'phone2' => $request->input('phone2'),
                'email' => $request->input('email'),
                'id_number' => $request->input('id_number'),
                'is_insurance' => $request->input('is_insurance'),
                'insurance_company' => $request->input('insurance_company'),
                'general_examination' => $request->input('general_examination'),
                'labs_examination' => $request->input('labs_examination'),
                'blood' => $request->input('blood'),
                'sensitivity' => $request->input('sensitivity'),
                'clinic_id' => $userr->clinic_id
                 ));
        if($patients->save()){
           // $patients = Patients::where('clinic_id', $userr->clinic_id)->get();
            return $patients;
        } else {
            return "unsuccessfully added patient :(";
        }
        }
        else {
            return $test;
        }
        
        } else // if not authorized 
        {
              return "you Are Not Authorized to perform this Action";
        }
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patients  $patients
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)//show normal visit
    {
        $userr = $this->request->user();
        $user = Auth::guard('api');
        if (Gate::allows('ViewVisitPatient', $user)) {
            /*$patients_visits = DB::table('Visits')
            ->join('Users', 'Visits.user_id', '=', 'Users.id')
            ->where('Visits.patient_id', $id)
            ->where('p1n0', 0)
            ->where('Visits.clinic_id', $userr->clinic_id)
            ->get();*/
       $patients_visits = Visits::where('patient_id', $id)->where('active',1)->where('clinic_id', $userr->clinic_id)->where('p1n0', 0)->get();
        if($patients_visits){
            return $patients_visits;
        } else {
            return "it's not exist";
        }
    }
    else {
        return 'you are not authorized  Visit';
    }
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patients  $patients
     * @return \Illuminate\Http\Response
     */
    public function edit(Patients $patients)
    {
        //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userr = $this->request->user();
        $test ="noth";
         $user = Auth::guard('api');
        if (Gate::allows('EditPatientDetails', $user)) {
        $patients = Patients::where('id',$id)->first();
        if($patients){
        if($request->input('patient_name') == $patients->patient_name && $request->input('gender') == $patients->gender && 
            $request->input('birth_date') == $patients->birth_date && $request->input('address') == $patients->address && 
            $request->input('phone1') == $patients->phone1 && $request->input('phone2') == $patients->phone2 && 
            $request->input('email') == $patients->email && $request->input('id_number') == $patients->id_number && 
            $request->input('insurance_company') == $patients->insurance_company && $request->input('general_examination') == $patients->general_examination &&
            $request->input('labs_examination') == $patients->labs_examination && $request->input('blood') == $patients->blood && 
            $request->input('sensitivity') == $patients->sensitivity 
            ){
            return $test;
        }
        else {
        $patients->patient_name = $request->input('patient_name');
        $patients->gender = $request->input('gender');
        $patients->birth_date = $request->input('birth_date');
        $patients->address = $request->input('address');
        $patients->phone1 = $request->input('phone1');
        $patients->phone2 = $request->input('phone2');
        $patients->email = $request->input('email');
        $patients->id_number = $request->input('id_number');
        $patients->insurance_company = $request->input('insurance_company');
        $patients->general_examination = $request->input('general_examination');
        $patients->labs_examination = $request->input('labs_examination');
        $patients->blood = $request->input('blood');
        $patients->sensitivity = $request->input('sensitivity');
        $patients->clinic_id = $userr->clinic_id;
        $patients->save();
        $patients = Patients::find($id)->where('clinic_id', $userr->clinic_id)->first();
        //return $patients;
        return "done";
        } 
        } else {
            return "it's not exist";
        }
    }
    else {
        return "you are not authorized to edit patient details";
    }
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)// show patient info to update them in editpatient page
    {
        $userr = $this->request->user();
        $user = Auth::guard('api');
        if (Gate::allows('ViewVisitPatient', $user)) {
        $patients = Patients::find($id)->where('clinic_id', $userr->clinic_id)->first();
        if($patients){
            return $patients;
        } else {
            return "it's not exist";
        }
        
    }
    else {
        return 'you are not authorized patientinfo';
    }
    }

    /******* TO return data of patient for Edit Patient component*******/

     public function getPatientData(Request $request, $id)
    {
        //
        $userr = $this->request->user();
         $user = Auth::guard('api');
         if (Gate::allows('EditPatientDetails', $user)) {
        $patients = Patients::where('id',$id)->where('clinic_id', $userr->clinic_id)->first();
        if($patients){
            return $patients;
        } else {
            return "it's not exist";
        }
        
    }
     else {
         return 'you are not authorized patientinfo';
     }
 }

  /******* TO return data of patient for medical report  component*******/

     public function getPatientData_medical(Request $request, $id)
    {
        //
         $user = Auth::guard('api');
         if (Gate::allows('AddMedicalReport', $user)) {
        $patients = Patients::find($id);
        if($patients){
            return $patients;
        } else {
            return "it's not exist";
        }
        
    }
     else {
         return 'you are not authorized patientinfo';
     }
 }
}
