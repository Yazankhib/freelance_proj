<?php

namespace App\Http\Controllers;

use App\Clinicalexaminations;
use App\Clinicalexamdetails;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
class clinicalexamdetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        $user_clinic = $this->request->user();
    
        $clinicalexamdetails = Clinicalexamdetails::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
        if($clinicalexamdetails->isEmpty()){
            return 'no Clinicalexaminations found !!';
        } else {
            return $clinicalexamdetails;
        }
    

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_clinic = $this->request->user();
        $test ="exist";
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $clinicalexaminations = Clinicalexaminations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->orderBy('id', 'desc')->first();
        if($clinicalexaminations){
            $clinicalexam_id = $clinicalexaminations->id;
        } else {
            return 'no Clinicalexaminations found !!';
        }
        for($i = 0; $i < count($request->input('name'));$i++){
            $clinicalexamdetails = Clinicalexamdetails::where('name', $request->input('name')[$i])->where('active',1)->where('clinic_id',$user_clinic->clinic_id)->where('clinicalexam_id',$clinicalexam_id)->get();            
            if(count($clinicalexamdetails) != 0){
                continue;
            } else {
                $clinicalexamdetails = Clinicalexamdetails::create(array(
                    'name' => $request->input('name')[$i],
                    'clinicalexam_id' => $clinicalexam_id,
                    'clinic_id' => $user_clinic->clinic_id,
                    'active' => '1',
                    ));
            if($clinicalexamdetails->save()){
                //$user = $this->request->user();
               // $clinicalexamdetails = Clinicalexamdetails::where('clinic_id',$user->clinic_id)->get();
                //$clinicalexamdetails = Clinicalexamdetails::all();
               // return $clinicalexamdetails;
            } else {
                return "unsuccessfully added clinicalexamdetails :(";
            }
            } 
        }
         $clinicalexamdetails = Clinicalexamdetails::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
                //$clinicalexamdetails = Clinicalexamdetails::all();
               return $clinicalexamdetails;

        }
        else {
            return "you are not authorized to add on clinical examination details";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clinicalexamdetails  $clinicalexamdetails
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {        
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
    	$clinicalexamdetails = Clinicalexamdetails::where('clinicalexam_id', $id)->where('active',1)->get();
        if($clinicalexamdetails->isEmpty()){
            return "it's empty";
        } else {
        	return $clinicalexamdetails;
        }
        }
        else 
    {
        return "you are not authorized to show clincal examinations details "; 
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clinicalexamdetails  $clinicalexamdetails
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clinicalexamdetails  $clinicalexamdetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_clinic = $this->request->user();
        $test ="exist";
        $tt = 0;
        $ids = [];
        $names = [];
          $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $clinicalexamdetailsoriginal = Clinicalexamdetails::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->where('clinicalexam_id',$id)->get();            
        for($s = 0; $s < count($clinicalexamdetailsoriginal); $s++){
            $clinicalexamdetail = Clinicalexamdetails::where('active',1)->find($clinicalexamdetailsoriginal[$s]->id);
            $clinicalexamdetail->delete();
        }   
        for($i = 0; $i < count($request->input('name'));$i++){
            $clinicalexamdetails = Clinicalexamdetails::where('name', $request->input('name')[$i])->where('active',1)->where('clinic_id',$user_clinic->clinic_id)->where('clinicalexam_id',$id)->get();            
            if(count($clinicalexamdetails) != 0){
                continue;
            } else {
                $clinicalexamdetails = Clinicalexamdetails::create(array(
                    'name' => $request->input('name')[$i],
                    'clinicalexam_id' => $id,
                    'clinic_id' => $user_clinic->clinic_id,
                    'active' => '1',
                    ));
            if($clinicalexamdetails->save()){
                //$user = $this->request->user();
               // $clinicalexamdetails = Clinicalexamdetails::where('clinic_id',$user->clinic_id)->get();
                //$clinicalexamdetails = Clinicalexamdetails::all();
               // return $clinicalexamdetails;
            } else {
                return "unsuccessfully added clinicalexamdetails :(";
            }
            } 
        }
         $clinicalexamdetails = Clinicalexamdetails::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
                //$clinicalexamdetails = Clinicalexamdetails::all();
               return $clinicalexamdetails;

        /*if(count($clinicalexamdetailsoriginal) == count($request->input('name'))){
            for($s = 0; $s < count($clinicalexamdetailsoriginal); $s++){
                    if($request->input('name')[$s] == $clinicalexamdetailsoriginal[$s]->name){
                        $tt = $tt +1;
                    } else {
                        $ids[] = $clinicalexamdetailsoriginal[$s]->id;
                        $names[] = $request->input('name')[$s];
                    }
            }
                for($r =0; $r <count($ids); $r++){
                    //return $ids[$r];
                    $clinicalexamdetail = Clinicalexamdetails::find($ids[$r]);
                    $clinicalexamdetail->name = $names[$r];
                    $clinicalexamdetail->clinic_id = $user->clinic_id;
                    $clinicalexamdetail->clinicalexam_id = $id;
                    $clinicalexamdetail->save();
                }
        } else if(count($clinicalexamdetailsoriginal) > count($request->input('name'))){
            
        } else if(count($clinicalexamdetailsoriginal) < count($request->input('name'))){
            
        }*/
        }
        else {
            return "you are not authorized to edit clinical examinations details";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clinicalexamdetails  $clinicalexamdetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clinicalexamdetails $clinicalexamdetails)
    {
        //
    }
}
