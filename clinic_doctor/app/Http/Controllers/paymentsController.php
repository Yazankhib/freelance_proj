<?php

namespace App\Http\Controllers;

use App\Payments;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
class paymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        //malak: unused
        /*$payments = Payments::all();
        if($payments->isEmpty()){
            return 'no payments found !!';
        } else {
            return $payments;
        }*/
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request) //malak: used in edit visit
    {
        $userr = $this->request->user();
        $test ="noth";
        $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        $payment = Payments::where('clinic_id' , $userr->clinic_id)->where('active',1)->get();
        foreach ($payment as $pay) {
            if($request->input('visit_id') == $pay->visit_id)
            {
                $test = "true";
                $payr = $pay;
            } 
        }
        if($test != "true")
        {
                $payments = Payments::create(array(
                'name' => 'name',
                'is_insurance' => '1',
                'insurance_company' => $request->input('insurance_company'),
                'amount' => $request->input('amount'),
                'payment' => $request->input('payment'),
                'labspayment' => $request->input('labspayment'),
                'testpayment' => $request->input('testpayment'),
                'anotherpayment' => $request->input('anotherpayment'),
                'active' => '1',
                'visit_id' => $request->input('visit_id'),
                'clinic_id' => $userr->clinic_id,
                 ));
        if($payments->save()){
            $payments = Payments::where('clinic_id' , $userr->clinic_id)->where('active',1)->get();
            return $payments;
        } else {
            return "unsuccessfully added payments :(";
        } 
    } else {
            if($payr->insurance_company == $request->input('insurance_company') && $payr->amount == $request->input('amount') &&
                $payr->payment == $request->input('payment') && $payr->labspayment == $request->input('labspayment') &&
                $payr->testpayment == $request->input('testpayment') && $payr->anotherpayment == $request->input('anotherpayment') &&
                $payr->visit_id == $request->input('visit_id'))
            {
                return "exist";
            }
            else {
            $payr->name = 'name';
            $payr->is_insurance = '1';
            $payr->insurance_company = $request->input('insurance_company');
            $payr->amount = $request->input('amount');
            $payr->payment = $request->input('payment');
            $payr->labspayment = $request->input('labspayment');
            $payr->testpayment = $request->input('testpayment');
            $payr->anotherpayment = $request->input('anotherpayment');
            $payr->active = '1';
            $payr->visit_id = $request->input('visit_id');
            $payr->clinic_id =  $userr->clinic_id;
            $payr->save();
            return "modified";
        }
        }
    
            
        /*if($request->input('visit_id') == $payment->visit_id)
        {
            return $test;
        } else {
        $payments = Payments::create(array(
                'name' => 'name',
                'is_insurance' => '1',
                'insurance_company' => $request->input('insurance_company'),
                'amount' => $request->input('amount'),
                'payment' => $request->input('payment'),
                'labspayment' => $request->input('labspayment'),
                'testpayment' => $request->input('testpayment'),
                'anotherpayment' => $request->input('anotherpayment'),
                'visit_id' => $request->input('visit_id'),
                'clinic_id' => $request->input('clinic_id')
                 ));
        if($payments->save()){
            $payments = Payments::all();
            return $payments;
        } else {
            return "unsuccessfully added payments :(";
        }
    }*/
    }
    else {
        return "you are not authorized to add on payments";
    }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Payments  $payments
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)//malak: return payments in editvisit and details visit
    {
        $userr = $this->request->user();
          $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        $payments = Payments::where('visit_id', $id)->where('active',1)->where('clinic_id' , $userr->clinic_id)->first();
        if($payments){
            return $payments;
        } else {
            return "it's not exist";
        }
    }
    else {
        return "you are not authorized to show  payments";
    }
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payments  $payments
     * @return \Illuminate\Http\Response
     */
    public function edit(Payments $payments)
    {
        //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payments  $payments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payments $payments)
    {
         //
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payments  $payments
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payments $payments)
    {
         //
    }
}
