<?php

namespace App\Http\Controllers;

use App\Clinicalexaminations_visits;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
use Ixudra\Curl\Facades\Curl;

class clinicalexaminations_visitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        $userr = $this->request->user();
        $clinicalexaminations_visits = Clinicalexaminations_visits::where('clinic_id' , $userr->clinic_id)->get();
        if($clinicalexaminations_visits->isEmpty()){
            return 'no Clinicalexaminations_visits found !!';
        } else {
            return $clinicalexaminations_visits;
        }
    }
 
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
    public function create()
    {
         //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//malak: used in edit visit
    {
        $userr = $this->request->user();
    	$test ="exist";
    	$user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
    	for($i =0; $i<count($request->input('clinicalexami_id')); $i++){
        $clinicalexaminations_visits = Clinicalexaminations_visits::where('visit_id', $request->input('visit_id'))->where('clinic_id' , $userr->clinic_id)->where('clinicalexami_id', $request->input('clinicalexami_id')[$i])->get();
        if($clinicalexaminations_visits->isEmpty())
        {
        	$clinicalexaminations_visits = Clinicalexaminations_visits::create(array(
            'clinicalexami_id' => $request->input('clinicalexami_id')[$i],
            'visit_id' => $request->input('visit_id'),
            'clinic_id' => $userr->clinic_id,
            'active' => '1'
            ));
        if($clinicalexaminations_visits->save()){
           // $clinicalexaminations_visits = Clinicalexaminations_visits::all();
            //return $clinicalexaminations_visits;
        } else {
            return "unsuccessfully added clinicalexaminations_visits test";
        }
        } else {
        	continue;
        	//return $test;
        }
    	}


    	/*$test ="exist";
        $clinicalexaminations_visits = Clinicalexaminations_visits::where('visit_id', $request->input('visit_id'))->where('clinicalexami_id', $request->input('clinicalexami_id'))->get();
        if($clinicalexaminations_visits->isEmpty())
        {
        $clinicalexaminations_visits = Clinicalexaminations_visits::create(array(
            'clinicalexami_id' => $request->input('clinicalexami_id'),
            'visit_id' => $request->input('visit_id'),
            'clinic_id' => $request->input('clinic_id')
                 ));
        if($clinicalexaminations_visits->save()){
            $clinicalexaminations_visits = Clinicalexaminations_visits::all();
            return $clinicalexaminations_visits;
        } else {
            return "unsuccessfully added clinicalexaminations_visits test :(";
        }
        } else {
            return $test;
        }*/
    }
    else {
        return "you are not authorized to add on clinical examination visists";
    }
    
    }
 
    /**
     * Display the specified resource.
     *
     * @param  \App\Clinicalexaminations_visits  $clinicalexaminations_visits
     * @return \Illuminate\Http\Response
     */
     public function show(Clinicalexaminations_visits $clinicalexaminations_visits)
     {
            $clinicalexamdetailsvisit = Examinationdetails_visits::where('visit_id',$id)->get();
        
        return $clinicalexamdetailsvisit;
     }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clinicalexaminations_visits  $clinicalexaminations_visits
     * @return \Illuminate\Http\Response
     */
    public function edit(Clinicalexaminations_visits $clinicalexaminations_visits)
    {
        //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clinicalexaminations_visits  $clinicalexaminations_visits
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clinicalexaminations_visits $clinicalexaminations_visits)
    {
         //
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clinicalexaminations_visits  $clinicalexaminations_visits
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clinicalexaminations_visits $clinicalexaminations_visits)
    {
         //
    }
    public function test($id){
        $msg =urlencode('ود تذكيرك بموعد زيارتك مع الدكتور زون في عيادة زون الساعة 9');
        
        //$msg =urlencode('9مساء نودتذكيركبالموعدعندالدكتورمممممالساعة');
        $phone =$id;
        $url ='http://sms.zone.ps/API/SendSMS.aspx?id=7cf225b4d5f4f30c84aee90010822d95&sender=Doc%20Net&to='.$phone.'&msg='.$msg.'&mode=0';
        $response = Curl::to($url)->get();
        //return $response;
        if($response == "Message Sent Successfully!"){
           return "success";
        } else {
            return $response;
        }
    // Send a GET request to: http://www.foo.com/bar?foz=baz
    // $response = Curl::to('http://sms.zone.ps/API/SendSMS.aspx?id=7cf225b4d5f4f30c84aee90010822d95&sender=Doc%20Net&to=970595587865&msg=malak&mode=0')
    //     ->withData( array( 'foz' => 'baz' ) )
    //     ->get();

    // // Send a GET request to: http://www.foo.com/bar?foz=baz using JSON
    // $response = Curl::to('http://sms.zone.ps/API/SendSMS.aspx?id=7cf225b4d5f4f30c84aee90010822d95&sender=Doc%20Net&to=970595587865&msg=malak&mode=0')
    //     ->withData( array( 'foz' => 'baz' ) )
    //     ->asJson()
    //     ->get();

    }
}
