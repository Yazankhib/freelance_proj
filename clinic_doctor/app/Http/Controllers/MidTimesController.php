<?php

namespace App\Http\Controllers;

use App\Midtimes;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
use App\vaccinations;
class MidTimesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userr = $this->request->user();
        $user = Auth::guard('api');
        if (Gate::allows('InsertMidtimesDetails', $user)) {
         $Midtimes = Midtimes::create(array(
                'patient_id' => $request->input('patient_id'),
                'mid_date' => $request->input('midDate'),
                'vaccinate_id' => $request->input('vaccinate_id'),
                'note' => $request->input('note'),
                'active' => '1',
				'clinic_id' => $userr->clinic_id
                ));
               // $Midtimes->save();
                 if($Midtimes->save()){
               // $Midtimes = Midtimes::where('clinic_id', $userr->clinic_id)->get();
               $Midtimes = Vaccinations::join('Midtimes', 'Midtimes.vaccinate_id', '=', 'Vaccinations.id') 
               ->where('Midtimes.patient_id', $request->input('patient_id'))
               ->where('vaccinations.clinic_id', $userr->clinic_id)
               ->where('Midtimes.clinic_id', $userr->clinic_id)
               ->where('vaccinations.active', 1)
               ->where('Midtimes.active', 1)
               ->get();
                return $Midtimes;
            } else {
                return "unsuccessfully added Midtimes :(";
            }
        }
        else {
            return "you are not authorized to insert on Midtimes";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Midtimes  $midtimes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $pid)
    {
        $userr = $this->request->user();
        $user = Auth::guard('api');
        if (Gate::allows('ViewMidtimesDetails', $user)) {
            $Midtimes = vaccinations::join('midtimes', 'midtimes.vaccinate_id', '=', 'vaccinations.id') 
            ->where('midtimes.patient_id', $pid)
            ->where('vaccinations.clinic_id', $userr->clinic_id)
            ->where('midtimes.clinic_id', $userr->clinic_id)
            ->where('vaccinations.active', 1)
            ->where('midtimes.active', 1)
            ->get();
       // $Midtimes = Midtimes::where('clinic_id', $userr->clinic_id)->get();
        if($Midtimes->isEmpty()){
            return 'no midTimes found !!';
        } else {
            return $Midtimes;
        }
        }
       else {
         return 'fail you are not authorized';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Midtimes  $midtimes
     * @return \Illuminate\Http\Response
     */
    public function edit(Midtimes $midtimes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Midtimes  $midtimes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Midtimes $midtimes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Midtimes  $midtimes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Midtimes $midtimes, $id)
    {
        $userr = $this->request->user();
          $user = Auth::guard('api');
        if (Gate::allows('InsertMidtimesDetails', $user)) {
        $midtimes = Midtimes::where('active',1)->where('clinic_id', $userr->clinic_id)->find($id);
        $patid = $midtimes->patient_id;
        if(!$midtimes){
            return "can not find midtimes";
        } else {
            $midtimes->active = '0';
            $midtimes->save();
            $midtimes = vaccinations::join('midtimes', 'midtimes.vaccinate_id', '=', 'vaccinations.id')
            ->where('vaccinations.clinic_id', $userr->clinic_id)
            ->where('midtimes.clinic_id', $userr->clinic_id)
            ->where('midtimes.patient_id', $patid)
            ->where('midtimes.active', 1)
            ->where('vaccinations.active', 1)
            ->get();
             return $midtimes;
        }
    }
    else {
        return "you are not authorized to delete from midtimes";
    }
    }
}
