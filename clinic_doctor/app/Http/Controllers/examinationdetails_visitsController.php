<?php

namespace App\Http\Controllers;

use App\Clinicalexaminations_visits;
use App\Clinicalexamdetails;
use App\Visits;
use App\Examinationdetails_visits;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
class examinationdetails_visitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }
     
    public function index()
    {
        //malak: unused
       /* $examinationdetails_visits = Examinationdetails_visits::all();
        if($examinationdetails_visits->isEmpty()){
            return 'no Examinationdetails_visits found !!';
        } else {
            return $examinationdetails_visits;
        }*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//malak: used to add details of clinicalexam in visit
    {
        $userr = $this->request->user();
        $test ="exist";
        $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        for($i =0; $i<count($request->input('exdetail_id')); $i++){
        $examinationdetails_visits = Examinationdetails_visits::where('visit_id', $request->input('visit_id'))->where('clinic_id' , $userr->clinic_id)->where('exdetail_id', $request->input('exdetail_id')[$i])->get();
        if($examinationdetails_visits->isEmpty())
        {
            $examinationdetails_visits = Examinationdetails_visits::create(array(
            'exdetail_id' => $request->input('exdetail_id')[$i],
            'visit_id' => $request->input('visit_id'),
            'clinic_id' => $userr->clinic_id,
            'active' => '1',
                 ));
        if($examinationdetails_visits->save()){
           // $examinationdetails_visits = Examinationdetails_visits::all();
            //return $examinationdetails_visits;
        } else {
            return "unsuccessfully added examinationdetails_visits test";
        }
        } else {
            continue;
            //return $test;
        }
        }
    	/*$test ="exist";
        // $clinicalexamdetailsvisit = "";
        $clinicalexamiforeigns = Clinicalexaminations_visits::where('visit_id', $request->input('visit_id'))->get();
        foreach ($clinicalexamiforeigns as $clinicalexamiforeign){
        $clinicalexamdetails[] = Clinicalexamdetails::where('clinicalexam_id', $clinicalexamiforeign->clinicalexami_id)->get();
        }
        for ($i =0 ; $i<count($clinicalexamdetails); $i++){
            for ($j =0 ; $j<count($clinicalexamdetails[$i]); $j++){
                if($clinicalexamdetails[$i][$j]->id == $request->input('exdetail_id')){
                    $test ="trueadd";
                    break 2; 
                    //echo $test;
                    //echo $clinicalexamdetails[$i][$j]->id;
                }
                else {
                    $test = "canotadd";
                }
            }
        }
        //return $test;
        if($test == "trueadd"){
            $examinationdetails_visits = Examinationdetails_visits::where('visit_id', $request->input('visit_id'))->where('exdetail_id', $request->input('exdetail_id'))->get();
        //return $examinationdetails_visits;
        if($examinationdetails_visits->isEmpty())
        {
            $examinationdetails_visits = Examinationdetails_visits::create(array(
            'exdetail_id' => $request->input('exdetail_id'),
            'visit_id' => $request->input('visit_id'),
            'clinic_id' => $request->input('clinic_id')
                 ));
        if($examinationdetails_visits->save()){
            $examinationdetails_visits = Examinationdetails_visits::all();
            return $examinationdetails_visits;
        } else {
            return "unsuccessfully added Examinationdetails_visits :(";
        }
        } else {
            
            return "exist";

        }
        } else {
        return "canotjjadd";
    }
        return $test;*/

        /*$visits = Visits::find($request->input('visit_id'));
        $clinicalexamdetails = Clinicalexamdetails::where('clinicalexam_id', $visits->clinicalexamination_id)->get();
        //return $clinicalexamdetails;


        foreach ($clinicalexamdetails as $clinicalexamdetail) {
          //  $clinicalexamdetailsvisit[] = Examinationdetails_visits::where('visit_id',$request->input('visit_id'))->where('exdetail_id', $clinicalexamdetail->id)->get();
            //echo $clinicalexamdetail->id;
            if($request->input('exdetail_id') == $clinicalexamdetail->id){
                $test = "true";
            }
        }
        if($test == "true"){
            $examinationdetails_visits = Examinationdetails_visits::where('visit_id', $request->input('visit_id'))->where('exdetail_id', $request->input('exdetail_id'))->get();
        //return $examinationdetails_visits;
        if($examinationdetails_visits->isEmpty())
        {
            $examinationdetails_visits = Examinationdetails_visits::create(array(
            'exdetail_id' => $request->input('exdetail_id'),
            'visit_id' => $request->input('visit_id'),
            'clinic_id' => $request->input('clinic_id')
                 ));
        if($examinationdetails_visits->save()){
            $examinationdetails_visits = Examinationdetails_visits::all();
            return $examinationdetails_visits;
        } else {
            return "unsuccessfully added Examinationdetails_visits :(";
        }
        } else {
            
            return "exist";

        }
        } else {
        return "canotadd";
    }*/


       /* $examinationdetails_visits = Examinationdetails_visits::where('visit_id', $request->input('visit_id'))->where('exdetail_id', $request->input('exdetail_id'))->get();
        if($examinationdetails_visits->isEmpty())
        {
        $examinationdetails_visits = Examinationdetails_visits::create(array(
        	'exdetail_id' => $request->input('exdetail_id'),
            'visit_id' => $request->input('visit_id'),
            'clinic_id' => $request->input('clinic_id')
                 ));
        if($examinationdetails_visits->save()){
            $examinationdetails_visits = Examinationdetails_visits::all();
            return $examinationdetails_visits;
        } else {
            return "unsuccessfully added Examinationdetails_visits :(";
        }
        } else {
        	return $test;

        }*/
        }
        else {
            return "you are not authorized to add on examinational detials of visits";
        }
    }
/**
     * Display the specified resource.
     *
     * @param  \App\Examinationdetails_visits  $examinationdetails_visits
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)//malak: show details of clinical in viewvisit and editvisit 
    {   
        $userr = $this->request->user();
        $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        $clinicalexamdetailsvisit = "";
        //$visits = Visits::where('id', $id)->first();
       // $clinicalexamdetails = Clinicalexamdetails::where('clinicalexam_id', $visits->clinicalexamination_id)->get();
        //foreach ($clinicalexamdetails as $clinicalexamdetail) {
            $clinicalexamdetailsvisit = Examinationdetails_visits::where('visit_id',$id)->where('clinic_id' , $userr->clinic_id)->get();
       // }
        return $clinicalexamdetailsvisit;
        }
        else {
            return "you are not authorized to show clinical examination on visits";
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Examinationdetails_visits  $examinationdetails_visits
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Examinationdetails_visits  $examinationdetails_visits
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Examinationdetails_visits  $examinationdetails_visits
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $data)
    {

    }
    public function delete_detail_visit($visitid, $exdetailid){

        $examinationdetails_visits = Examinationdetails_visits::where('visit_id', $visitid)->where('exdetail_id', $exdetailid)->first();
        if($examinationdetails_visits){
        $examinationdetails_visits->delete($visitid, $exdetailid);
        } else {
            return "notexist";
        }
    }

}
