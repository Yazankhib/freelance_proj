<?php

namespace App\Http\Controllers;

use App\Symptoms;
use Illuminate\Http\Request;

class symptomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $symptoms = Symptoms::all();
        if($symptoms->isEmpty()){
            return 'no symptoms found !!';
        } else {
            return $symptoms;
        }
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $symptoms = Symptoms::create(array(
                'name' => $request->input('name'),
                'clinic_id' => $request->input('clinic_id')
                 ));
        if($symptoms->save()){
            $symptoms = Symptoms::all();
            return $symptoms;
        } else {
            return "unsuccessfully added symptom :(";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Symptoms  $symptoms
     * @return \Illuminate\Http\Response
     */
    public function show(Symptoms $symptoms)
    {
         //
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Symptoms  $symptoms
     * @return \Illuminate\Http\Response
     */
    public function edit(Symptoms $symptoms)
    {
        //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Symptoms  $symptoms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Symptoms $symptoms)
    {
         //
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Symptoms  $symptoms
     * @return \Illuminate\Http\Response
     */
    public function destroy(Symptoms $symptoms)
    {
         //
    }
}
