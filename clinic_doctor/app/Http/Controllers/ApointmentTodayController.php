<?php

namespace App\Http\Controllers;
use App\Visits;
use Illuminate\Http\Request;
use App\User;
use App\Visitlabs;
use App\Payments;
use App\Visitrays;
use App\Patients;
use Auth;
use Gate;

use Illuminate\Support\Facades\DB;

class ApointmentTodayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        $user = $this->request->user();    
        $now_str = \Carbon\Carbon::now()->format('Y-m-d');
        //dd($now_str);
       /* $visits= Visits::where('revisit_date','>=',$now_str)->where('clinic_id', $user->clinic_id)->get();

        if($visits->isEmpty()){
            return 'no visits found !!';
        } else {
            return $visits;
        }*/
        $nextvisits = patients::join('visits', 'visits.patient_id', '=', 'patients.id')
        ->where('visits.revisit_date','=',$now_str)
        ->where('visits.active','=','1')
        ->where('visits.clinic_id', $user->clinic_id)
        ->where('patients.clinic_id', $user->clinic_id)
        ->get();
    return $nextvisits;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->request->user();
        $visits = Visits::where('active',1)->where('p1n0', 1)->find($id);
        $visitlabs = Visitlabs::where('visit_id', $id)->where('active',1)->where('clinic_id' , $user->clinic_id)->get();
        $visitrays = Visitrays::where('visit_id', $id)->where('active',1)->where('clinic_id' , $user->clinic_id)->get();
        $payments = Payments::where('visit_id', $id)->where('active',1)->where('clinic_id' , $user->clinic_id)->first();
        
        $pid = $visits->patient_id;
        if(!$visits){
            return "can not find lab";
        } else {
            if(!$visitlabs->isEmpty()){
                foreach ($visitlabs as $visitlab) {
                        $visitlab->active = '0';
                        $visitlab->save();
                } 
            }
            if(!$visitrays->isEmpty()){
                foreach ($visitrays as $visitray) {
                        $visitray->active = '0';
                        $visitray->save();
                } 
            }
            if($payments){
                $payments->active = '0';
                $payments->save();
            }
            $visits->active = '0';
            $visits->save();
            $visits = Visits::where('clinic_id',$user->clinic_id)->where('patient_id',$pid)->where('active',1)->where('p1n0', 1)->get();
            return $visits;
        }
    }
    public function edittodayrevisit(Request $request, $id) {
        $userr = $this->request->user(); 
        $now_str = \Carbon\Carbon::now()->format('Y-m-d');       
        $test ="noth";
        $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        $visit = Visits::where('active',1)->find($id);
        if($request->input('in_time') == $visit->in_time &&
        $request->input('out_time') == $visit->out_time &&
         $request->input('visit_date') == $visit->visit_date && 
         $request->input('revisit_date') == $visit->revisit_date&& 
         $request->input('revisit_time') == $visit->revisit_time){
            return $test;
        }
        else {
        $visit->patient_id = $request->input('patient_id');
        $visit->revisit_time = $request->input('revisit_time');
        $visit->visit_date = $request->input('visit_date');
        $visit->revisit_date = $request->input('revisit_date');
        $visit->active = '1';
        $visit->in_time = $request->input('in_time');
        $visit->out_time = $request->input('out_time');
        $visit->user_id = $request->input('user_id');
        $visit->save();
        
        $visits = patients::join('visits', 'visits.patient_id', '=', 'patients.id')
        ->where('visits.revisit_date','=',$now_str)
        ->where('visits.active','=','1')
        ->where('visits.clinic_id', $userr->clinic_id)
        ->where('patients.clinic_id', $userr->clinic_id)
        ->get();
        return $visits;
    }
      }
      else {
          return "you are not authorized to edit currnet visit";
      }
    }
}
