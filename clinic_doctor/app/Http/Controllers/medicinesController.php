<?php

namespace App\Http\Controllers;

use App\Medicines;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;


class medicinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        $user_clinic = $this->request->user();
        $medicines = Medicines::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
        if($medicines->isEmpty()){
            return 'no Medicines found !!';
        } else {
            return $medicines;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_clinic = $this->request->user();
        $test ="exist";
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $medicines = Medicines::where('active',1)->where('name', $request->input('name'))->where('clinic_id',$user_clinic->clinic_id)->
        where('active_name', $request->input('active_name'))->where('company', $request->input('company'))
        ->where('number', $request->input('number'))->where('amount', $request->input('amount'))->get();
        if($medicines->isEmpty()){
            $medicines = Medicines::create(array(
                'name' => $request->input('name'),
                'active_name' => $request->input('active_name'),
                'company' => $request->input('company'),
                'number' => $request->input('number'),
                'amount' => $request->input('amount'),
                'clinic_id' => $user_clinic->clinic_id,
                'active'=> '1',
                ));
        if($medicines->save()){
            $user_clinic = $this->request->user();
            $medicines = Medicines::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            //$medicines = Medicines::all();
            return $medicines;
        } else {
            return "unsuccessfully added Medicines :(";
        }
    } else {
        return $test;
    }     
    }
    else {
        return "you are not authorized to add on medicine";
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medicines  $medicines
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $medicines = Medicines::where('active',1)->find($id);
        if($medicines){
            return $medicines;
        } else {
            return "not found";
        }
    }
    else {
        return "you are not authorized to add on medicine";
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Medicines  $medicines
     * @return \Illuminate\Http\Response
     */
    public function edit(Medicines $medicines)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medicines  $medicines
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_clinic = $this->request->user();
        $test ="noth";
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $medicines = Medicines::find($id);
      //  $allmedicines = Medicines::all();
        $allmedicines = Medicines::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();

        if($request->input('editname') == $medicines->name && $request->input('editnumber') == $medicines->number && 
        $request->input('editamount') == $medicines->amount && $request->input('editactive_name') == $medicines->active_name &&
        $request->input('editcompany') == $medicines->company){
            return $test;
        }
        else {
            foreach($allmedicines as $allmedicine){
                if($allmedicine->name == $request->input('editname') && $allmedicine->active_name == $request->input('editactive_name') && 
                $allmedicine->number == $request->input('editnumber') && $allmedicine->amount == $request->input('editamount') && 
                $allmedicine->company == $request->input('editcompany')){
                    return "exist";
                } else {
                    $test ="ss";
                }
            }
            if($test == "ss"){
                $medicines->name = $request->input('editname');
                $medicines->active_name = $request->input('editactive_name');
                $medicines->number = $request->input('editnumber');
                $medicines->amount = $request->input('editamount');
                $medicines->company = $request->input('editcompany');
                $medicines->clinic_id = $user_clinic->clinic_id;
                $medicines->active = '1';
                $medicines->save();
                //$medicines = Medicines::all();
                $medicines = Medicines::where('clinic_id',$user_clinic->clinic_id)->where('active', 1)->get();
                return $medicines;
            }
        }
    }
    else {
        return "you are not authorized to edit medicine";
    }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medicines  $medicines
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medicines $medicines, $id)
    {
        $user_clinic = $this->request->user();
        $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $medicines = Medicines::where('active',1)->where('clinic_id',$user_clinic->clinic_id)->find($id);
        if(!$medicines){
            return "can not find medicine";
        } else {
            $medicines->active = '0';
            $medicines->save();
            $medicines = Medicines::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            return $medicines;
        }

    }
    else {
        return "you are not authorized to delete medicine";
    }
    }
}
