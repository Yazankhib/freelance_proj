<?php

namespace App\Http\Controllers;

use App\Clinics;
//use GuzzleHttp\Client;
use App\Http\Controllers\GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
class clinicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        /* // comment this, to check it after where we will need it 
        $Clinics = Clinics::all();
        if($Clinics->isEmpty()){
            return 'no Clinics found !!';
        } else {
            return $Clinics;
        }*/

        $user_clinic = $this->request->user();
         $clinics = Clinics::where('id', $user_clinic->clinic_id)->first();
        if(!$clinics){//->isEmpty()
            return "not found";
        } else {
            return $clinics->SmsCount ;//->SmsCount;
        }

    }
 
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
    public function create()
    {
         //
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Clinics = Clinics::create(array(
                'name' => $request->input('name'),
                'clinic_id' => $request->input('clinic_id')
                 ));
        if($Clinics->save()){
            $Clinics = Clinics::all();
            return $Clinics;
        } else {
            return "unsuccessfully added Clinic :(";
        }
    }
 
    /**
     * Display the specified resource.
     *
     * @param  \App\Clinics  $clinics
     * @return \Illuminate\Http\Response
     */
     public function show(Clinics $clinics)
     {
         //
          
     }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clinics  $clinics
     * @return \Illuminate\Http\Response
     */
    public function edit(Clinics $clinics)
    {
        //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clinics  $clinics
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clinics $clinics)
    {
         //
        //  $client = new GuzzleHttp\Client();
        // $res = $client->get('https://api.github.com/users/mralexgray/repos');
        //dd($res->getBody());
        //return "obada";//$res->getBody() ;
        // echo $res->getStatusCode(); // 200
        // echo $res->getBody(); 
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clinics  $clinics
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clinics $clinics)
    {
         //
    }

     public function updateSmsCount(Clinics $clinics)
    {
         //
         $user_clinic = $this->request->user();
         $Clincs_SmsCount = Clinics::find($user_clinic->clinic_id);
         $Remaining_SmsCount = $Clincs_SmsCount ->SmsCount ;
         $clinics = Clinics::where('id', $user_clinic->clinic_id)->first();
         if ( $Remaining_SmsCount > 0) {
         $clinics->SmsCount = $Remaining_SmsCount - 1 ; 
        // $Clinics = Clinics::create(array(
        //         'name' => $request->input('name'),
        //         'clinic_id' => $request->input('clinic_id')
        //          ));
        if($clinics->save()){
            //$clinics = Clinics::all();
            return 'Decrement Success';//$clinics;
        } else {
            return "unsuccessfully added Clinic :(";
        }
        }
        else {
            return "you don't have enough messages in your account"; 
        }
    }
    /* public function getDat(Clinics $clinics)
    {
         //
    $client = new GuzzleHttp\Client();
        $res = $client->get('https://api.github.com/user', [
         'auth' =>  ['user', 'pass']
            ]);
        echo $res->getStatusCode();           // 200
            echo $res->getHeader('content-type'); // 'application/json; charset=utf8'
        echo $res->getBody();                 // {"type":"User"...'
        var_export($res->json());   
         return "obada";
    // $client = new \Guzzle\Service\Client('https://api.github.com/users/mralexgray/repos');
    // $response = $client->get()->send();
    // dd($response);
    }
    */
}