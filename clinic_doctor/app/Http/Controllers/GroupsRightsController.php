<?php

namespace App\Http\Controllers;

use App\GroupsRights;
use Illuminate\Http\Request;

class GroupsRightsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $userr = $this->request->user();
       
        for($i =0; $i<count($request->input('right_id')); $i++){
        /*$visitlabs = Visitlabs::where('visit_id', $request->input('visit_id'))->where('lab_id', $request->input('lab_id')[$i])->get();
        if($visitlabs->isEmpty())
        {*/
        $groups_rights = GroupsRights::create(array(
            'group_id' => $request->input('group_id'),
            'right_id' => $request->input('right_id')[$i],
            'clinic_id' => $userr->clinic_id
                 ));
        if($groups_rights->save()){
            $groups_rights = GroupsRights::where('clinic_id',$userr->clinic_id)->get();
           return 'insert group right';
        } else {
            return "unsuccessfully added group right :(";
        }
        } 
        /*else {
            continue;
            return 'test';
        }*/
    //}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GroupsRights  $groupsRights
     * @return \Illuminate\Http\Response
     */
    public function show(GroupsRights $groupsRights)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GroupsRights  $groupsRights
     * @return \Illuminate\Http\Response
     */
    public function edit(GroupsRights $groupsRights)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GroupsRights  $groupsRights
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupsRights $groupsRights)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GroupsRights  $groupsRights
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupsRights $groupsRights)
    {
        //
    }
}
