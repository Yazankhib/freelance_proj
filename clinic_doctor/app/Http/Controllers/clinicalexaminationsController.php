<?php

namespace App\Http\Controllers;

use App\Visits;
use App\Clinicalexaminations;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Clinicalexamdetails;
use Auth;
use Gate;

class clinicalexaminationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {

        $user_clinic = $this->request->user();
        $clinicalexaminations = Clinicalexaminations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
        if($clinicalexaminations->isEmpty()){
            return 'no Clinicalexaminations found !!';
        } else {
            return $clinicalexaminations;
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $user_clinic = $this->request->user();
        $test ="exist";
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $clinicalexaminations = Clinicalexaminations::where('type', $request->input('type'))->where('active',1)->where('clinic_id',$user_clinic->clinic_id)->get();
        if($clinicalexaminations->isEmpty()){
        $clinicalexaminations = Clinicalexaminations::create(array(
                'type' => $request->input('type'),
                'clinic_id' => $user_clinic->clinic_id,
                'active' => '1',
                ));
        if($clinicalexaminations->save()){
            $user_clinic = $this->request->user();
            $clinicalexaminations = Clinicalexaminations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            //$clinicalexaminations = Clinicalexaminations::all();
            return $clinicalexaminations;
            
        } else {
            return "unsuccessfully added Clinic :(";
        }
    } else {
        return $test;
    } 
        }
        return "you are not authorized to add on clinical examinations";     
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clinicalexaminations  $clinicalexaminations
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $clinicalexaminations = Clinicalexaminations::where('active',1)->find($id);
        if($clinicalexaminations){
            return $clinicalexaminations;
        } else {
            return "not found";
        }
    }
    else {
        return "you are not authorized to show clinical examinations";
    }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clinicalexaminations  $clinicalexaminations
     * @return \Illuminate\Http\Response
     */
    public function edit(Clinicalexaminations $clinicalexaminations)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clinicalexaminations  $clinicalexaminations
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_clinic = $this->request->user();
        $test ="noth";
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $clinicalexaminations = Clinicalexaminations::where('active',1)->find($id);
      //  $allclinicalexaminations = clinicalexaminations::all();
        $allclinicalexaminations = Clinicalexaminations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();

        if($request->input('edittype') == $clinicalexaminations->type){
            return $test;
        }
        else {
            foreach($allclinicalexaminations as $allclinicalexamination){
                if($allclinicalexamination->type == $request->input('edittype')){
                    return "exist";
                } else {
                    $test ="ss";
                }
            }
            if($test == "ss"){
                $clinicalexaminations->type = $request->input('edittype');
                $clinicalexaminations->clinic_id = $user_clinic->clinic_id;
                $clinicalexaminations->active = '1';
                $clinicalexaminations->save();
                //$clinicalexaminations = Clinicalexaminations::all();
                $clinicalexaminations = Clinicalexaminations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
                return $clinicalexaminations;
            }
        }
    }
    else {
        return "you are not authourized to edit clinical examinations";
    }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clinicalexaminations  $clinicalexaminations
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clinicalexaminations $clinicalexaminations, $id)
    {
        $user_clinic = $this->request->user();
          $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $clinicalexaminations = Clinicalexaminations::where('active',1)->find($id);
        $clinicalexamdetails = Clinicalexamdetails::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->where('clinicalexam_id',$id)->get();            
        

        if(!$clinicalexaminations){
            return "can not find lab";
        } else {
            $clinicalexaminations->active = '0';
            $clinicalexaminations->save();
            $clinicalexaminations = Clinicalexaminations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            for($s = 0; $s < count($clinicalexamdetails); $s++){
                $clinicalexamdetail = Clinicalexamdetails::find($clinicalexamdetails[$s]->id);
                $clinicalexamdetail->active = '0';
                $clinicalexamdetail->save();
            }
            return $clinicalexaminations;
        }
    }
    else {
                return "you are not authourized to delete clinical examinations";
    }
    }
}
