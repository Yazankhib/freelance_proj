<?php

namespace App\Http\Controllers;

use App\Visits;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
class visitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        //malak: unused
        /*$visits = Visits::all();
        if($visits->isEmpty()){
            return 'no visits found !!';
        } else {
            return $visits;
        }*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_clinic = $this->request->user();        
        $test ="exist";
        $user = Auth::guard('api');
        if (Gate::allows('addNewVisitPatient', $user)) {
        $visit = Visits::where('revisit_time', $request->input('revisit_time'))->where('active',1)->Where('revisit_date', $request->input('revisit_date'))->get();
        if($visit->isEmpty()){
            $visits = Visits::create(array(
                'patient_id' => $request->input('patient_id'),
                'complaints' => $request->input('complaints'),
                'lab_test_id' => $request->input('lab_test_id'),
                'ray_test_id' => $request->input('ray_test_id'),
                'revisit_time' => $request->input('revisit_time'),
                'diagnosis' => $request->input('diagnosis'),
                'recom_treat' => $request->input('recom_treat'),
                'final_diagnosis' => $request->input('final_diagnosis'),
                'visit_date' => $request->input('visit_date'),
                'revisit_date' => $request->input('revisit_date'),
                'ask_for_help' => $request->input('ask_for_help'),
                'age' => $request->input('age'),
                'active' => '1',
                'in_time' => $request->input('in_time'),
                'out_time' => $request->input('out_time'),
                'user_id' => $request->input('user_id'),
                'weight' => $request->input('weight'),
                'item_p' => $request->input('item_p'),
                'item_tem' => $request->input('item_tem'),
                'item_bp' => $request->input('item_bp'),
                'item_wt' => $request->input('item_wt'),
                'item_rr' => $request->input('item_rr'),
                'note' => $request->input('note'),
                'symptoms' => $request->input('symptoms'),
                'clinic_id' => $user_clinic->clinic_id,
                'p1n0' => $request->input('p1n0'),
                /*'embryoweeknumbers' => $request->input('embryoweeknumbers'),
                'embryostate' => $request->input('embryostate'),
                'uterusheight' => $request->input('uterusheight'),
                'embryopulse' => $request->input('embryopulse'),
                'swelling' => $request->input('swelling'),
                'weightpreg' => $request->input('weightpreg'),
                'pressure' => $request->input('pressure'),
                'ALBtest' => $request->input('ALBtest'),
                'Gluctest' => $request->input('Gluctest'),
                'BPD' => $request->input('BPD'),
                'FL' => $request->input('FL'),
                'AC' => $request->input('AC'),
                'EFbwt' => $request->input('EFbwt'),
                'Placenta' => $request->input('Placenta'),
                'Abnormalities' => $request->input('Abnormalities'),
                'embryoweight' => $request->input('embryoweight'),
                'embryoheight' => $request->input('embryoheight'),
                'embryoheadcircumference' => $request->input('embryoheadcircumference'),*/
                ));
            if($visits->save()){
                $visits = Visits::all();
                return $visits;
            } else {
                return "unsuccessfully added visit :(";
            }
        } else {
            return $test;
        }
        }
        else {
            return "you are not authorized to add new visit";
        }

    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Visits  $visits
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)//return pregnant visit
    {
        $userr = $this->request->user();
        $user = Auth::guard('api');
        if (Gate::allows('ViewVisitPatient', $user)) {
            
        $patients_visits = Visits::where('patient_id', $id)->where('active',1)->where('clinic_id', $userr->clinic_id)->where('p1n0', 1)->get();
        if($patients_visits){
            return $patients_visits;
        } else {
            return "it's not exist";
        }
    }
    else {
        return 'you are not authorized  Visit';
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visits  $visits
     * @return \Illuminate\Http\Response
     */
    public function edit(Visits $visits)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visits  $visits
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userr = $this->request->user();        
        $test ="noth";
        $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        $visit = Visits::where('active',1)->find($id);
        if($request->input('diagnosis') == $visit->diagnosis && $request->input('patient_id') == $visit->patient_id && 
            $request->input('complaints') == $visit->complaints && $request->input('diagnosis') == $visit->diagnosis && 
            $request->input('recom_treat') == $visit->recom_treat && $request->input('visit_date') == $visit->visit_date && 
            $request->input('revisit_date') == $visit->revisit_date && $request->input('ask_for_help') == $visit->ask_for_help && 
            $request->input('note') == $visit->note && $request->input('symptoms') == $visit->symptoms && $request->input('item_p')==
            $visit->item_p && $request->input('item_tem') == $visit->item_tem && $request->input('item_bp') == $visit->item_bp && 
            $request->input('item_wt') == $visit->item_wt && $request->input('item_rr') == $visit->item_rr && $visit->embryoweeknumbers == 
            $request->input('embryoweeknumbers') && $visit->embryostate == $request->input('embryostate') && $visit->uterusheight ==
            $request->input('uterusheight') && $visit->embryopulse == $request->input('embryopulse') && $visit->swelling == $request->input('swelling') 
            && $visit->weightpreg == $request->input('weightpreg') && $visit->pressure == $request->input('pressure') && 
            $visit->pressure == $request->input('pressure') && $visit->ALBtest == $request->input('ALBtest') && $visit->Gluctest == $request->input('Gluctest')
            && $visit->BPD == $request->input('BPD') && $visit->FL == $request->input('FL') && $visit->AC == $request->input('AC') && 
            $visit->EFbwt == $request->input('EFbwt') && $visit->Placenta == $request->input('Placenta') && $visit->Abnormalities == $request->input('Abnormalities') &&
            $visit->embryoweight == $request->input('embryoweight') && $visit->embryoheadcircumference == $request->input('embryoheadcircumference')
            ){
            return $test;
        }
        else {
        $visit->patient_id = $request->input('patient_id');
        $visit->complaints = $request->input('complaints');
        $visit->revisit_time = $request->input('revisit_time');
        $visit->diagnosis = $request->input('diagnosis');
        $visit->recom_treat = $request->input('recom_treat');
        $visit->visit_date = $request->input('visit_date');
        $visit->revisit_date = $request->input('revisit_date');
        $visit->ask_for_help = $request->input('ask_for_help');
        $visit->age = $request->input('age');
        $visit->active = '1';
        $visit->in_time = $request->input('in_time');
        $visit->out_time = $request->input('out_time');
        $visit->user_id = $request->input('user_id');
        $visit->weight = $request->input('weight');
        $visit->item_p = $request->input('item_p');
        $visit->item_tem = $request->input('item_tem');
        $visit->item_bp = $request->input('item_bp');
        $visit->item_wt = $request->input('item_wt');
        $visit->item_rr = $request->input('item_rr');
        $visit->note = $request->input('note');
        $visit->symptoms = $request->input('symptoms');
        $visit->clinic_id = $userr->clinic_id;
        $visit->p1n0 = $request->input('p1n0');
        $visit->embryoweeknumbers = $request->input('embryoweeknumbers');
        $visit->embryostate = $request->input('embryostate');
        $visit->uterusheight = $request->input('uterusheight');
        $visit->embryopulse = $request->input('embryopulse');
        $visit->swelling = $request->input('swelling');
        $visit->weightpreg = $request->input('weightpreg');
        $visit->pressure = $request->input('pressure');
        $visit->ALBtest = $request->input('ALBtest');
        $visit->Gluctest = $request->input('Gluctest');
        $visit->BPD = $request->input('BPD');
        $visit->FL = $request->input('FL');
        $visit->AC = $request->input('AC');
        $visit->EFbwt = $request->input('EFbwt');
        $visit->Placenta = $request->input('Placenta');
        $visit->Abnormalities = $request->input('Abnormalities');
        $visit->embryoweight = $request->input('embryoweight');
        $visit->embryoheight = $request->input('embryoheight');
        $visit->embryoheadcircumference = $request->input('embryoheadcircumference');
        $visit->save();
        $visit = Visits::where('active',1)->find($id);
        return $visit;
    }
      }
      else {
          return "you are not authorized to edit currnet visit";
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $visits = Visits::find($id);
        if($visits){
            return $visits;
        } else {
            return "it's not exist";
        }
    }   
}
