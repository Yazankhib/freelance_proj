<?php

namespace App\Http\Controllers;

use App\Labs;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;

class labsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }
     
    public function index() 
    {
        //return all Labs
        /*
        $labs_info ;
        $labs = DB::table('Labs')->get(); 
        foreach ($labs as $lab) {//malak: get banks information
            $sql="select * from Labs";
            $labs_info = DB::select($sql);//malak: execute select statment
           // dd($banks_info);
          // return $banks_info;
        }*/
        $user_clinic = $this->request->user();
        $labs_info = Labs::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
        if($labs_info->isEmpty()){
            return 'no labs found !!';
        } else {
            return $labs_info;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$data = Input::all();
       /* $labs = Labs::create(array(
                'name' => Input::get('name'),
				'clinic_id' => Input::get('clinic_id')
                ));
        if($labs->save()){
            $labs_info = Labs::all();
            return $labs_info ;//$labs_info;
        } else {
            return "unsuccessfully added lab :(";
        }*/
        $user_clinic = $this->request->user();
        $test ="exist";
             $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $labs = Labs::where('name', $request->input('name'))->where('active',1)->where('clinic_id',$user_clinic->clinic_id)->get();
        if($labs->isEmpty()){
        $labs = Labs::create(array(
                'name' => $request->input('name'),
                'clinic_id' => $user_clinic->clinic_id,
                'active' => '1',
                ));
        if($labs->save()){
            $user_clinic = $this->request->user();
            $labs = Labs::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            //$birthtypes = labs::all();
            return $labs;
        } else {
            return "unsuccessfully added lab :(";
        }
    } else {
        return $test;
    }
    }else {
        return "you are not authorized to add on labs";
    }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Labs  $labs
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
             $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $labs = Labs::where('active',1)->find($id);
        if($labs){
            return $labs;
        } else {
            return "not found";
        }
    }else {
        return "you are not authorized to show the labs";
    }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Labs  $labs
     * @return \Illuminate\Http\Response
     */
    public function edit(Labs $labs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Labs  $labs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_clinic = $this->request->user();
        $test ="noth";
             $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $labs = Labs::where('active',1)->find($id);
      //  $alllabs = Labs::all();
        $alllabs = Labs::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();

        if($request->input('editname') == $labs->name){
            return $test;
        }
        else {
            foreach($alllabs as $alllab){
                if($alllab->name == $request->input('editname')){
                    return "exist";
                } else {
                    $test ="ss";
                }
            }
            if($test == "ss"){
                $labs->name = $request->input('editname');
                $labs->clinic_id = $user_clinic->clinic_id;
                $lab->active = '1';
                $labs->save();
                //$labs = Labs::all();
                $labs = Labs::where('clinic_id',$user_clinic->clinic_id)->where('active', 1)->get();
                return $labs;
            }
        }
    }else {
        return "you are not authorized to edit labs";
    }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Labs  $labs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Labs $labs, $id)
    {
        $user_clinic = $this->request->user();
          $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $labs = Labs::where('active',1)->find($id);
        if(!$labs){
            return "can not find lab";
        } else {
            $labs->active = '0';
            $labs->save();
            $labs = Labs::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            return $labs;
        }
    }
    else {
        return "you are not authorized to delete labs";
    }
    }
}
