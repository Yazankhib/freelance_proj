<?php

namespace App\Http\Controllers;

use App\BirthTypes;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;

class birthtypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        //$birthTypes = BirthTypes::all();
        $user_clinic = $this->request->user();
        $birthTypes = BirthTypes::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
        if($birthTypes->isEmpty()){
            return 'no birth types found';
        } else {
            return $birthTypes;
        }
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_clinic = $this->request->user();
        $test ="exist";
            $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $birthtypes = BirthTypes::where('name', $request->input('name'))->where('active',1)->where('clinic_id',$user_clinic->clinic_id)->get();
        if($birthtypes->isEmpty()){
        $birthtypes = BirthTypes::create(array(
                'name' => $request->input('name'),
                'clinic_id' => $user_clinic->clinic_id,
                'active' => '1',
                ));
        if($birthtypes->save()){
            $user_clinic = $this->request->user();
            $birthTypes = BirthTypes::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            //$birthtypes = BirthTypes::all();
            return $birthTypes;
        } else {
            return "unsuccessfully added birthtypes :(";
        }
    } else {
        return $test;
    }
        }
        else {
            return "you are not authorized to add on birthtypes";
        }
    }
 
    /**
     * Display the specified resource.
     *
     * @param  \App\BirthTypes  $birthtypes
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
            $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $birthtypes = BirthTypes::where('active',1)->find($id);
        if($birthtypes){
            return $birthtypes;
        } else {
            return "not found";
        }
    }else {
        return "you are not authorized to show bithrtypes";
    }
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BirthTypes  $birthtypes
     * @return \Illuminate\Http\Response
     */
    public function edit(BirthTypes $birthtypes)
    {
         //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BirthTypes  $birthtypes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_clinic = $this->request->user();
        $test ="noth";
            $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $birthtypes = BirthTypes::where('active',1)->find($id);
      //  $allbirthtypes = BirthTypes::all();
        $allbirthtypes = BirthTypes::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();

        if($request->input('editname') == $birthtypes->name){
            return $test;
        }
        else {
            foreach($allbirthtypes as $allbirthtype){
                if($allbirthtype->name == $request->input('editname')){
                    return "exist";
                } else {
                    $test ="ss";
                }
            }
            if($test == "ss"){
                $birthtypes->name = $request->input('editname');
                $birthtypes->clinic_id = $user_clinic->clinic_id;
                $birthtypes->active = '1';
                $birthtypes->save();
                //$birthtypes = BirthTypes::all();
                $birthtypes = BirthTypes::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
                return $birthtypes;
            }
        }
    }else {
        return "you are not authorized to edit the bithtypes";
    }
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BirthTypes  $birthtypes
     * @return \Illuminate\Http\Response
     */
    public function destroy(BirthTypes $birthtypes, $id)
    {
        $user_clinic = $this->request->user();
              $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $birthtypes = BirthTypes::where('active',1)->find($id);
        if(!$birthtypes){
            return "can not find lab";
        } else {
            $birthtypes->active = '0';
            $birthtypes->save();
            $birthtypes = BirthTypes::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            return $birthtypes;
        }
    }
    else {
                return "you are not authorized to remove the bithtypes";

    }
    }
}
