<?php

namespace App\Http\Controllers;

use App\diagnoses;
use Illuminate\Http\Request;

class diagnosesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diagnoses = diagnoses::all();
        if($diagnoses->isEmpty()){
            return 'no diagnoses found !!';
        } else {
            return $diagnoses;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       // $data = Input::all();
        $diagnoses = diagnoses::create(array(
                'name' => $request->input('name'),
				'clinic_id' => $request->input('clinic_id')
                ));
        if($diagnoses->save()){
            $diagnoses = diagnoses::all();
            return $diagnoses;
        } else {
            return "unsuccessfully added diagnoses :(";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\diagnoses  $diagnoses
     * @return \Illuminate\Http\Response
     */
    public function show(diagnoses $diagnoses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\diagnoses  $diagnoses
     * @return \Illuminate\Http\Response
     */
    public function edit(diagnoses $diagnoses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\diagnoses  $diagnoses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, diagnoses $diagnoses)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\diagnoses  $diagnoses
     * @return \Illuminate\Http\Response
     */
    public function destroy(diagnoses $diagnoses)
    {
        //
    }
}
