<?php

namespace App\Http\Controllers;

use App\Complaints;
use Illuminate\Http\Request;

class complaintsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $complaints = Complaints::all();
        if($complaints->isEmpty()){
            return 'no complaints found !!';
        } else {
            return $complaints;
        }
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $complaints = Complaints::create(array(
                'name' => $request->input('name'),
                'clinic_id' => $request->input('clinic_id')
                 ));
        if($complaints->save()){
            $complaints = Complaints::all();
            return $complaints;
        } else {
            return "unsuccessfully added complaint :(";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Complaints  $complaints
     * @return \Illuminate\Http\Response
     */
    public function show(Complaints $clinics)
    {
         //
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Complaints  $complaints
     * @return \Illuminate\Http\Response
     */
    public function edit(Complaints $complaints)
    {
        //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Complaints  $complaints
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Complaints $complaints)
    {
         //
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Complaints  $complaints
     * @return \Illuminate\Http\Response
     */
    public function destroy(Complaints $complaints)
    {
         //
    }
}
