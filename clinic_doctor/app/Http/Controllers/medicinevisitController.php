<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Medicinevisit;

class medicinevisitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        //malak: unused
        /*$visitlabs = Visitlabs::all();
        if($visitlabs->isEmpty()){
            return 'no labs found !!';
        } else {
            return $visitlabs;
        }*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//malak: add lab to visit
    {
        $user_clinic = $this->request->user();
        $test ="exist";
         $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        for($i =0; $i<count($request->input('medicine_id')); $i++){
        $medicinevisit = Medicinevisit::where('visit_id', $request->input('visit_id'))->where('medicine_id', $request->input('medicine_id')[$i])->get();
        if($medicinevisit->isEmpty())
        {
        $medicinevisit = Medicinevisit::create(array(
            'medicine_id' => $request->input('medicine_id')[$i],
            'visit_id' => $request->input('visit_id'),
            'clinic_id' => '1',
            'active' => '1'
                 ));
        if($medicinevisit->save()){
            $medicinevisit = Medicinevisit::all();
           // return $medicinevisit;
        } else {
            return "unsuccessfully added medicine  :(";
        }
        } else {
            continue;
            return $test;
        }
    }
        }
        else {
            return "you are not authorized to add on medicine visit";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visitlabs  $visitlabs
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)//malak: show all medicines of visit
    {
        $user_clinic = $this->request->user();
        $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        $medicinevisit = Medicinevisit::where('active',1)->where('visit_id', $id)->where('clinic_id', $user_clinic->clinic_id)->get();
        if(!$medicinevisit->isEmpty()){
            return $medicinevisit;
        } else {
            return "not found";
        }
        }
        else {
            return "you are not authorized to add on medicine visit";
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visitlabs  $visitlabs
     * @return \Illuminate\Http\Response
     */
    public function edit(Visitlabs $visitlabs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visitlabs  $visitlabs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visitlabs $visitlabs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visitlabs  $visitlabs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitlabs $visitlabs)
    {
        //
    }
}
