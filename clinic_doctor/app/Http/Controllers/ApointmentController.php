<?php

namespace App\Http\Controllers;
use App\Visits;
use Illuminate\Http\Request;
use App\User;
use App\Visitlabs;
use App\Patients;
use App\Payments;
use App\Visitrays;
use App\Medicinevisit;
use App\Clinicalexaminations_visits;
use App\Examinationdetails_visits;
use Illuminate\Support\Facades\DB;
use Ixudra\Curl\Facades\Curl;
use Auth;
use Gate;

class ApointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        $user = $this->request->user();    
        $now_str = \Carbon\Carbon::now()->format('Y-m-d');
        //dd($now_str);
       /* $visits= Visits::where('revisit_date','>=',$now_str)->where('clinic_id', $user->clinic_id)->get();

        if($visits->isEmpty()){
            return 'no visits found !!';
        } else {
            return $visits;
        }*/
        $nextvisits = patients::join('visits', 'visits.patient_id', '=', 'patients.id')
        ->join('users', 'users.id', '=', 'visits.user_id')
        ->where('visits.revisit_date','>=',$now_str)
        ->where('patients.clinic_id', $user->clinic_id)
        ->where('visits.clinic_id', $user->clinic_id)
        ->where('visits.active','=','1')
        ->get();
        return $nextvisits;
    }
    /*****Api For Send SMS******/
    public function SendSMS ($id) 
    {       
        
        $msg =urlencode('ود تذكيرك بموعد زيارتك مع الدكتور زون في عيادة زون الساعة 9');
        $phone =$id;
        $url ='http://sms.zone.ps/API/SendSMS.aspx?id=7cf225b4d5f4f30c84aee90010822d95&sender=Doc%20Net&to='.$phone.'&msg='.$msg.'&mode=0';
        $response = Curl::to($url)->get();
        //return $response;
        if($response == "Message Sent Successfully!"){
           return "success";
        } else {
            return $response;
        }
    }
    /**
     * Show the form for creating a new resource.
    
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)//to delete visit
    {
        $user = $this->request->user();
        $visits = Visits::where('active',1)->where('p1n0', 0)->find($id);
        $visitlabs = Visitlabs::where('visit_id', $id)->where('active',1)->where('clinic_id' , $user->clinic_id)->get();
        $visitrays = Visitrays::where('visit_id', $id)->where('active',1)->where('clinic_id' , $user->clinic_id)->get();
        $payments = Payments::where('visit_id', $id)->where('active',1)->where('clinic_id' , $user->clinic_id)->first();
        $visitmedicines = Medicinevisit::where('visit_id', $id)->where('active',1)->where('clinic_id' , $user->clinic_id)->get();
        $visitclinicals = Clinicalexaminations_visits::where('visit_id', $id)->where('active',1)->where('clinic_id' , $user->clinic_id)->get();
        $visitclinicalsdetails = Examinationdetails_visits::where('visit_id', $id)->where('active',1)->where('clinic_id' , $user->clinic_id)->get();
        
        $pid = $visits->patient_id;
        if(!$visits){
            return "can not find lab";
        } else {
            if(!$visitlabs->isEmpty()){
                foreach ($visitlabs as $visitlab) {
                        $visitlab->active = '0';
                        $visitlab->save();
                } 
            }
            if(!$visitrays->isEmpty()){
                foreach ($visitrays as $visitray) {
                        $visitray->active = '0';
                        $visitray->save();
                } 
            }
            if($payments){
                $payments->active = '0';
                $payments->save();
            }
            if(!$visitmedicines->isEmpty()){
                foreach ($visitmedicines as $visitmedicine) {
                        $visitmedicine->active = '0';
                        $visitmedicine->save();
                } 
            }
            if(!$visitclinicals->isEmpty()){
                foreach ($visitclinicals as $visitclinical) {
                        $visitclinical->active = '0';
                        $visitclinical->save();
                } 
            }
            if(!$visitclinicalsdetails->isEmpty()){
                foreach ($visitclinicalsdetails as $visitclinicalsdetail) {
                        $visitclinicalsdetail->active = '0';
                        $visitclinicalsdetail->save();
                } 
            }
            $visits->active = '0';
            $visits->save();
            $visits = Visits::where('clinic_id',$user->clinic_id)->where('patient_id',$pid)->where('active',1)->where('p1n0', 0)->get();
            return $visits;
        }
    }

    public function editallrevisit(Request $request, $id) {
        $userr = $this->request->user(); 
        $now_str = \Carbon\Carbon::now()->format('Y-m-d');       
        $test ="noth";
        $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        $visit = Visits::where('active',1)->find($id);
        if($request->input('in_time') == $visit->in_time &&
         $request->input('visit_date') == $visit->visit_date && 
         $request->input('revisit_date') == $visit->revisit_date){
            return $test;
        }
        else {
        $visit->patient_id = $request->input('patient_id');
        $visit->revisit_time = $request->input('revisit_time');
        $visit->visit_date = $request->input('visit_date');
        $visit->revisit_date = $request->input('revisit_date');
        $visit->active = '1';
        $visit->in_time = $request->input('in_time');
        $visit->out_time = $request->input('out_time');
        $visit->user_id = $request->input('user_id');
        $visit->save();
        
        $visits = patients::join('visits', 'visits.patient_id', '=', 'patients.id')
        ->where('visits.revisit_date','>=',$now_str)
        ->where('patients.clinic_id', $visit->clinic_id)
        ->where('visits.clinic_id', $visit->clinic_id)
        ->where('visits.active','=','1')
        ->get();
        return $visits;
    }
      }
      else {
          return "you are not authorized to edit currnet visit";
      }
    }
}