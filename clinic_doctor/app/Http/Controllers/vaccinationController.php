<?php

namespace App\Http\Controllers;

use App\vaccinations;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;

class vaccinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index() 
    {
        $user_clinic = $this->request->user();
        $Vaccinations = vaccinations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
       // $Vaccinations = vaccinations::all();
        if($Vaccinations->isEmpty()){
            return 'no vaccinations found';
        } else {
            return $Vaccinations;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_clinic = $this->request->user();
        $test ="exist";
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $Vaccinations = vaccinations::where('name', $request->input('name'))->where('active',1)->where('clinic_id',$user_clinic->clinic_id)->get();
        if($Vaccinations->isEmpty()){
        $Vaccinations = vaccinations::create(array(
                'name' => $request->input('name'),
                'clinic_id' => $user_clinic->clinic_id,
                'active' => '1',
                ));
        if($Vaccinations->save()){
            //$Vaccinations = vaccinations::all();
            $user_clinic = $this->request->user();
            $Vaccinations = vaccinations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            return $Vaccinations;
        } else {
            return "unsuccessfully added vaccinations";
        }
    } else {
        return $test;
    }         
    } else {
        return "you are not authorized to add on vaccinations";
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\vaccinations  $Vaccinations
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $Vaccinations = vaccinations::where('active',1)->find($id);
        if($Vaccinations){
            return $Vaccinations;
        } else {
            return "not found";
        }
    }else {
        return "you are not authorized to show vaccinations";
    }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Childmids  $childmids
     * @return \Illuminate\Http\Response
     */
    public function edit(Childmids $childmids)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Childmids  $childmids
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_clinic = $this->request->user();
        $test ="noth";
         $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $Vaccinations = vaccinations::where('active',1)->find($id);
      //  $allVaccinations = vaccinations::all();
        $allVaccinations = vaccinations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();

        if($request->input('editname') == $Vaccinations->name){
            return $test;
        }
        else {
            foreach($allVaccinations as $allVaccination){
                if($allVaccination->name == $request->input('editname')){
                    return "exist";
                } else {
                    $test ="ss";
                }
            }
            if($test == "ss"){
                $Vaccinations->name = $request->input('editname');
                $Vaccinations->clinic_id = $user->clinic_id;
                $Vaccinations->save();
                //$Vaccinations = vaccinations::all();
                $Vaccinations = vaccinations::where('clinic_id',$user->clinic_id)->where('active',1)->get();
                return $Vaccinations;
            }
        }
    }else {
        return "you are not authorzied to edit vaccinations";
    }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $user_clinic = $this->request->user();
        $user = Auth::guard('api');
        if (Gate::allows('Definitions', $user)) {
        $Vaccinations = vaccinations::where('active',1)->find($id);
        if(!$Vaccinations){
            return "can not find lab";
        } else {
            $Vaccinations->active = '0';
            $Vaccinations->save();
            $Vaccinations = vaccinations::where('clinic_id',$user_clinic->clinic_id)->where('active',1)->get();
            return $Vaccinations;
        }
    }
    else {
        return "you are not authorzied to delete vaccinations";
    }
    }
}
