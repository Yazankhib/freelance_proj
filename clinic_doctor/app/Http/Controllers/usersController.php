<?php

namespace App\Http\Controllers;

use App\User;
use App\Groups;
use App\Clinics;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use Auth;
use Gate;

class usersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {   
        $userr = $this->request->user();
        $users = User::where('clinic_id',$userr->clinic_id)->where('active', 1)->get();
        if($users->isEmpty()){
            return 'no users found !!';
        } else {
            return $users;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     
    public function store(Request $request)
    {
        $user_clinic = $this->request->user();
         $user = Auth::guard('api');
        if (Gate::allows('InsertUsersInClinic', $user)) {
        $alluser = User::where('email', $request->input('email'))->get();
        if($alluser->isEmpty()){
        $users = User::create(array(
                'name' => $request->input('user_name'),
                'user_name' => $request->input('user_name'),
                'clinic_address' => $request->input('clinic_address'),
                'home_address' => $request->input('home_address'),
                'email' => $request->input('email'),
                'clinic_phone' => $request->input('clinic_phone'),
                'home_phone' => $request->input('home_phone'),
                'mobile1' => $request->input('mobile1'),
                'mobile2' => $request->input('mobile2'),
                'specialization' => $request->input('specialization'),
                'password' => bcrypt($request->input('password')),
                'group_id' => $request->input('group_id'),
				'clinic_id' => $user_clinic->clinic_id,
                'active' => '1',
                ));
        if($users->save()){
            $users = User::where('clinic_id',$user_clinic->clinic_id)->where('active', 1)->get();
            return $users;
        } else {
            return "unsuccessfully added user";
        }
    } else {
        return "exist";
    }
    }
    else {
        return "you are Not Authorized to add users on Clinics";
    }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $user = Auth::guard('api');
        $Userd = User::where('active',1)->find($id);
        if($Userd){
            return $Userd;
        } else {
            return "not found";
        }
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_clinic = $this->request->user();
        $test ="noth";
        $Userd = User::where('active',1)->find($id);
        if($request->input('name') == $Userd->name && $Userd->email == $request->input('email') &&
        $Userd->clinic_phone == $request->input('clinic_phone') && $Userd->mobile1 == $request->input('mobile1') &&
        $Userd->mobile2 == $request->input('mobile2') && $Userd->specialization == $request->input('specialization') &&
         $Userd->group_id == $request->input('group_id') 
){ 
    return $test;
       
        } else {
             $Userd->name = $request->input('name');
             $Userd->email = $request->input('email');
             $Userd->clinic_phone = $request->input('clinic_phone');
             $Userd->mobile1 = $request->input('mobile1');
             $Userd->mobile2 = $request->input('mobile2');
             $Userd->specialization = $request->input('specialization');
             $Userd->password = bcrypt($request->input('password'));
             $Userd->group_id = $request->input('group_id');
		     $Userd->clinic_id = $user_clinic->clinic_id;
             $Userd->active = '1';
             if($Userd->save()){
                $Userdd = User::where('clinic_id', $user_clinic->clinic_id)->where('active',1)->get();
                return $Userdd;      
             }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $this->request->user();
        $Userd = User::where('active',1)->where('clinic_id',$user->clinic_id)->find($id);
        if(!$Userd){
            return "can not find user";
        } else {
            $Userd->active = '0';
            $Userd->save();
            $Userd = User::where('clinic_id',$user->clinic_id)->where('active',1)->get();
            return $Userd;
        }
    }

    public function adminregister(Request $request){
          $user = Auth::guard('api');
        if (Gate::allows('InsertUsersForAdmin', $user)) {
        $group_id = Groups::where('name', 'Admin')->first()->id;
        $allclinic = Clinics::where('name', $request->input('clinic_name'))->get();
        if($allclinic->isEmpty()){
            $clinic = Clinics::create(array(
                'name' => $request->input('clinic_name'),
                'SmsCount' => $request->input('SmsCount'),
            ));
            if($clinic->save()){
                $clinic_id =  $clinic->id;
                $users = User::create(array(
                    'name' => $request->input('username'),
                    'user_name' => $request->input('username'),
                    'clinic_address' => $request->input('clinic_address'),
                    'home_address' => $request->input('home_address'),
                    'email' => $request->input('email'),
                    'clinic_phone' => $request->input('clinic_phone'),
                    'home_phone' => $request->input('home_phone'),
                    'mobile1' => $request->input('mobile1'),
                    'mobile2' => $request->input('mobile2'),
                    'specialization' => $request->input('specialization'),
                    'password' => bcrypt($request->input('password')),
                    'group_id' => $group_id,
                    'clinic_id' => $clinic_id,
                    ));
            if($users->save()){
                return "successaddadmin";
            } else {
                return "unsuccessfully added admin :(";
            }
            } else {
                return "unsuccessfully added clinic :(";
            }
        } else {
            return "clinicexist";
        }
    }else {
        return "you Don't have Permision To Add Clinics";
    }
    }
}
