<?php

namespace App\Http\Controllers;

use App\Childmids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class childmidsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $childmids = Childmids::all();
        if($childmids->isEmpty()){
            return 'no child mids found';
        } else {
            return $childmids;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$data = Input::all();
        $childmids = Childmids::create(array(
                'name' => Input::get('name'),
				'clinic_id' => Input::get('clinic_id')
                ));
        if($childmids->save()){
            $childmids = Childmids::all();
            return $childmids;
        } else {
            return "unsuccessfully added child mid :(";
        }         
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Childmids  $childmids
     * @return \Illuminate\Http\Response
     */
    public function show(Childmids $childmids)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Childmids  $childmids
     * @return \Illuminate\Http\Response
     */
    public function edit(Childmids $childmids)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Childmids  $childmids
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Childmids $childmids)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $childmids = Childmids::find($id);
        if($childmids){
        $childmids->delete();
        } else {
            return "it's not exist";
        }
        $childmids = Childmids::all();
        return $childmids;
    }
}
