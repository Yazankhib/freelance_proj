<?php

namespace App\Http\Controllers;

use App\Cdc;
use Illuminate\Http\Request;

class cdcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cdc = Cdc::all();
       if($cdc->isEmpty()){
           return 'no cdc found';
       } else {
       return $cdc;
       }
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cdc = Cdc::create(array(
                'births_id' => $request->input('births_id'),
                'weight' => $request->input('weight'),
                'cdc_date' => $request->input('cdc_date'),
                'length' => $request->input('length'),
                'head' => $request->input('head'),
                'visit_id' => $request->input('visit_id'),
                'clinic_id' => $request->input('clinic_id')
                 ));
        if($cdc->save()){
            $cdc = Cdc::all();
            return $cdc;
        } else {
            return "unsuccessfully added cdc :(";
        }    
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Cdc  $cdc
     * @return \Illuminate\Http\Response
     */
    public function show(Cdc $cdc)
    {
         //
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cdc  $cdc
     * @return \Illuminate\Http\Response
     */
    public function edit(Cdc $cdc)
    {
        //
    }
 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cdc  $cdc
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cdc $cdc)
    {
         //
    }
 
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $cdc = Cdc::find($id);
        if($cdc){
        $cdc->delete();
        } else {
            return "it's not exist";
        }
        $cdc = Cdc::all();
        //Session::flash('message', 'Successfully delete cdc');
        return $cdc;
    }
}
