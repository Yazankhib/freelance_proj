<?php

namespace App\Http\Controllers;

use App\Labs;
use App\Visitlabs;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
class visitlabsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

    public function index()
    {
        //malak: unused
        /*$visitlabs = Visitlabs::all();
        if($visitlabs->isEmpty()){
            return 'no labs found !!';
        } else {
            return $visitlabs;
        }*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//malak: add lab to visit
    {
        $userr = $this->request->user();
        $test ="exist";
        $user = Auth::guard('api');
        if (Gate::allows('EditVisitPatient', $user)) {
        for($i =0; $i<count($request->input('lab_id')); $i++){
        $visitlabs = Visitlabs::where('visit_id', $request->input('visit_id'))->where('active',1)->where('clinic_id' , $userr->clinic_id)->where('lab_id', $request->input('lab_id')[$i])->get();
        if($visitlabs->isEmpty())
        {
        $visitlabs = Visitlabs::create(array(
            'lab_id' => $request->input('lab_id')[$i],
            'visit_id' => $request->input('visit_id'),
            'active' => '1',
            'clinic_id' => $userr->clinic_id
                 ));
        if($visitlabs->save()){
            $visitlabs = Visitlabs::where('clinic_id' , $userr->clinic_id)->where('active',1);
           // return $visitlabs;
        } else {
            return "unsuccessfully added lab test";
        }
        } else {
            continue;
            //return $test;
        }
    }
        }
        else {
            return "you are not authorized to add on visit labs";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visitlabs  $visitlabs
     * @return \Illuminate\Http\Response
     */
    public function show($id)//malak: show all labs of visit
    {
        $userr = $this->request->user();
        $labname = "";
        $visitlabs = Visitlabs::where('visit_id', $id)->where('active',1)->where('clinic_id' , $userr->clinic_id)->get();
        if(!$visitlabs->isEmpty()){
        foreach ($visitlabs as $visitlab) {
            $labn = Labs::where('id', $visitlab->lab_id)->where('active',1)->where('clinic_id' , $userr->clinic_id)->get();
            if(!$labn->isEmpty()){
                $labname[] = $labn;
            } 
        }
        return $labname;
    }
    else {
        return "no tests lab";
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visitlabs  $visitlabs
     * @return \Illuminate\Http\Response
     */
    public function edit(Visitlabs $visitlabs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visitlabs  $visitlabs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visitlabs $visitlabs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visitlabs  $visitlabs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitlabs $visitlabs)
    {
        //
    }

    public function delete_lab_visit($visitid, $labid){

        $visitlabs = Visitlabs::where('visit_id', $visitid)->where('lab_id', $labid)->first();
        if($visitlabs){
        $visitlabs->delete($visitid, $labid);
        } else {
            return "notexist";
        }
    }
}
