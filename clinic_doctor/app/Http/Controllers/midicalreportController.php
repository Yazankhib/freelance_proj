<?php

namespace App\Http\Controllers;

use App\medicalreports;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;
use App\User;
use Auth;
use Gate;
class midicalreportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected $request;
     public function __construct(Request $request) {
     $this->request = $request;
     }

     public function index()
     {
         //
         $Medicalreports = medicalreports::all();
         if($Medicalreports->isEmpty()){
             return 'no Medicalreports found !!';
         } else {
             return $Medicalreports;
         }
     }
 
     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //
     }
 
     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */

     public function store(Request $request)
     {
         //
         $user_clinic = $this->request->user();  
          $user = Auth::guard('api');
        if (Gate::allows('AddMedicalReport', $user)) {
         $Medicalreports = medicalreports::create(array(
                 'patient_id' => $request->input('patient_id'),
                 'date' => $request->input('date'),
                 'gender' => $request->input('gender'),
                 'notes' => $request->input('notes'),
                 'clinic_id' => $user_clinic->clinic_id
                 ));
         if($Medicalreports->save()){
           //  $Medicalreports = medicalreports::all();
           //  return $Medicalreports;
           return "success";
         } else {
             return "unsuccessfully added Medicalreports :(";
         }
        }
        else {
            return "you are not authorized to make medical report";
        }
     }
 
     /**
      * Display the specified resource.
      *
      * @param  \App\Medicines  $medicines
      * @return \Illuminate\Http\Response
      */
     public function show(Medicines $medicines)
     {
         //
     }
 
     /**
      * Show the form for editing the specified resource.
      *
      * @param  \App\Medicines  $medicines
      * @return \Illuminate\Http\Response
      */
     public function edit(Medicines $medicines)
     {
         //
     }
 
     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  \App\Medicines  $medicines
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, Medicines $medicines)
     {
         //
     }
 
     /**
      * Remove the specified resource from storage.
      *
      * @param  \App\Medicines  $medicines
      * @return \Illuminate\Http\Response
      */
     public function destroy(Medicines $medicines)
     {
         //
     }
}
