<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class diagnoses extends Model
{
    protected $table = 'diagnoses';
    protected $fillable = ['id', 'name', 'clinic_id'];
}
