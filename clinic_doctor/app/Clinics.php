<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinics extends Model
{
    protected $table = 'clinics';
    protected $fillable = ['id', 'name','SmsCount'];
}
