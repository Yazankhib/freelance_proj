<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medicalreports extends Model
{
    protected $table = 'medicalreports';
    protected $fillable = ['id', 'patient_id', 'date', 'gender', 'notes', 'clinic_id'];   
}