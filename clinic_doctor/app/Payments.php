<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table = 'payments';
    protected $fillable = ['id', 'name', 'labspayment', 'anotherpayment', 'active', 'testpayment', 'is_insurance', 'insurance_company', 'payment_date', 'amount', 'payment', 'visit_id', 'clinic_id'];
}
 