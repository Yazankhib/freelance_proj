<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitlabs extends Model
{
    protected $table = 'visitlabs';
    protected $fillable = ['id', 'lab_id', 'visit_id', 'active', 'clinic_id'];
}
