<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Xrays extends Model
{
    protected $table = 'xrays';
    protected $fillable = ['id', 'name', 'active', 'clinic_id'];
}
