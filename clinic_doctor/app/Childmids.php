<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Childmids extends Model
{
    protected $table = 'childmids';
    protected $fillable = ['id', 'name', 'clinic_id'];
}
