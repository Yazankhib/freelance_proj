<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Labs extends Model
{
    protected $table = 'labs';
    protected $fillable = ['id', 'name', 'active', 'clinic_id'];
}
