<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Symptoms extends Model
{
    protected $table = 'symptoms';
    protected $fillable = ['id', 'name', 'clinic_id'];
}
