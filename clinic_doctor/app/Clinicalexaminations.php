<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinicalexaminations extends Model
{
    protected $table = 'clinicalexaminations';
    protected $fillable = ['id','type', 'active', 'clinic_id'];
}
