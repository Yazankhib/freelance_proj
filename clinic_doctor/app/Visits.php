<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visits extends Model
{
    protected $table = 'visits';
    protected $fillable = ['id', 'patient_id', 'complaints', 'diagnosis', 'recom_treat', 'final_diagnosis', 'visit_date', 
    'revisit_date', 'ask_for_help', 'active', 'age', 'revisit_time', 'in_time', 'out_time', 'user_id', 'weight', 'item_p', 
    'item_tem', 'item_bp', 'item_wt', 'item_rr', 'note', 'symptoms', 'clinic_id', 'p1n0', 'embryoweeknumbers',
    'embryostate', 'uterusheight', 'embryopulse', 'swelling', 'weightpreg', 'pressure', 'ALBtest', 'Gluctest',
    'BPD', 'FL', 'AC', 'EFbwt', 'Placenta', 'AF', 'Abnormalities', 'embryoweight', 'embryoheight', 'embryoheadcircumference'];
}