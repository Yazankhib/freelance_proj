<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinicalexamdetails extends Model
{
    protected $table = 'clinicalexamdetails';
    protected $fillable = ['id', 'clinicalexam_id', 'name', 'active', 'clinic_id'];
}
