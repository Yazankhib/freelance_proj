<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vaccinations extends Model
{
    protected $table = 'vaccinations';
    protected $fillable = ['id', 'name', 'active','clinic_id'];
}
