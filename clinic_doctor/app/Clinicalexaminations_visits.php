<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinicalexaminations_visits extends Model
{
    protected $table = 'Clinicalexaminations_visits';
    protected $fillable = ['id', 'clinicalexami_id', 'visit_id', 'active', 'clinic_id'];

}
