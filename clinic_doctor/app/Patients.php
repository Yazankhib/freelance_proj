<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    protected $table = 'patients';
    protected $fillable = ['id', 'patient_name', 'gender', 'birth_date', 'address', 'phone1', 'phone2', 'email', 'id_number', 'is_insurance', 
    'general_examination', 'labs_examination', 'insurance_company', 'blood', 'sensitivity', 'clinic_id'];
}