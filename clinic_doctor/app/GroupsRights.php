<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupsRights extends Model
{
    protected $table = 'GroupsRights';// this fixed issue of take name of old table // obada 
    protected $fillable = [ 'group_id','right_id', 'clinic_id'];

}
