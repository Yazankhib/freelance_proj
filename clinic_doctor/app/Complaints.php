<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaints extends Model
{
    protected $table = 'complaints';
    protected $fillable = ['id', 'name', 'clinic_id'];
}
