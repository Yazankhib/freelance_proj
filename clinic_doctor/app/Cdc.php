<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cdc extends Model
{
    protected $table = 'cdcs';
    protected $fillable = ['id', 'births_id', 'weight', 'cdc_date', 'length', 'head', 'visit_id', 'clinic_id'];
}