<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rights extends Model
{
    protected $table = 'Rights';
    public function Groupes()
    {
        return $this->belongsToMany(Groupes::class);
    }
            protected $fillable = [ 'id','name'];

}
