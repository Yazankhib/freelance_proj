<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//obada
Route::resource('labs', 'labsController')->middleware('auth:api');
Route::resource('diagnoses', 'diagnosesController');
Route::resource('clinicalexaminations', 'clinicalexaminationsController')->middleware('auth:api');
Route::resource('xrays', 'XraysController')->middleware('auth:api');
Route::resource('users', 'usersController')->middleware('auth:api');
//malak
Route::resource('clinics', 'clinicsController')->middleware('auth:api');

Route::get('/decrementSmsCount','clinicsController@updateSmsCount')->middleware('auth:api');
Route::resource('birthtypes', 'birthtypesController')->middleware('auth:api');
Route::resource('cities', 'citiesController');
Route::resource('complaints', 'complaintsController');
Route::resource('medicines', 'medicinesController')->middleware('auth:api');
Route::resource('symptoms', 'symptomsController');
Route::resource('childmids', 'childmidsController');
Route::resource('visits', 'visitsController')->middleware('auth:api');
Route::resource('patients', 'patientsController')->middleware('auth:api');
Route::resource('payments', 'paymentsController')->middleware('auth:api');
Route::resource('cdc', 'cdcController');
Route::resource('MidTimes', 'MidTimesController')->middleware('auth:api');
Route::resource('midicalreport', 'midicalreportController')->middleware('auth:api');
Route::get('/check','checkController@check')->middleware('auth:api');
Route::get('/check','checkController@check')->middleware('auth:api');
Route::get('/UserPermisions','checkController@UserPermisions')->middleware('auth:api');
Route::get('/clinic_user','checkController@Clinic_User')->middleware('auth:api');
Route::resource('Groups', 'GroupsController');
Route::resource('Rights', 'RightsController');
Route::resource('GroupsRights', 'GroupsRightsController');
Route::resource('vaccination', 'vaccinationController')->middleware('auth:api');
Route::resource('medicinevisit', 'medicinevisitController')->middleware('auth:api');

Route::get('/testc','checkController@test');
/*{
    if (Gate::allow('test',Auth::user())){
        return 'obada Success';
    }else {
        return'you are not authorized';
    }
    return 'hala wallah';
});*/

Route::get('/test', function () {
   return response()->json([
    'name' => 'obada',
    'state' => 'abdelkareem'
]);
})->middleware('auth:api');
Route::resource('visitlabs', 'visitlabsController')->middleware('auth:api');
Route::resource('visitrays', 'visitraysController')->middleware('auth:api');
Route::resource('clinicalexamdetails', 'clinicalexamdetailsController')->middleware('auth:api');
Route::resource('examinationdetailsvisits', 'examinationdetails_visitsController')->middleware('auth:api');
Route::resource('clinicalexaminationsvisits', 'clinicalexaminations_visitsController')->middleware('auth:api');
Route::resource('visitlabs', 'visitlabsController')->middleware('auth:api');
Route::resource('visitrays', 'visitraysController')->middleware('auth:api');
Route::resource('clinicalexamdetails', 'clinicalexamdetailsController')->middleware('auth:api');
Route::resource('examinationdetailsvisits', 'examinationdetails_visitsController')->middleware('auth:api');
Route::resource('clinicalexaminationsvisits', 'clinicalexaminations_visitsController')->middleware('auth:api');
//Route::get('/test', 'clinicalexaminations_visitsController@test');
Route::delete('/examinationdetailsvisit/{id1}/{id2}', 'examinationdetails_visitsController@delete_detail_visit');
Route::delete('/visitlab/{id1}/{id2}', 'visitlabsController@delete_lab_visit');//malak: we dont need this api
Route::delete('/visitray/{id1}/{id2}', 'visitraysController@delete_ray_visit');//malak: we dont need this api
Route::get('/PatientData/{id}','patientsController@getPatientData')->middleware('auth:api');
Route::get('/PatientDataMedical/{id}','patientsController@getPatientData_medical')->middleware('auth:api');
Route::resource('RevisitPatientDate', 'ApointmentController')->middleware('auth:api');
Route::get('/sendSms/{id}', 'ApointmentController@SendSMS'); // For Send SMS for Apointment
Route::resource('RevisitPatientToday', 'ApointmentTodayController')->middleware('auth:api');
Route::post('adminregister', 'usersController@adminregister')->middleware('auth:api');

Route::post('editallrevisit/{id}', 'ApointmentController@editallrevisit')->middleware('auth:api');
Route::post('edittodrevisit/{id}', 'ApointmentTodayController@edittodayrevisit')->middleware('auth:api');
